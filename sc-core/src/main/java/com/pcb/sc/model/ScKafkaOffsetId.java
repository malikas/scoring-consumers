package com.pcb.sc.model;

import com.google.common.base.Objects;

import java.io.Serializable;

public class ScKafkaOffsetId implements Serializable {
    private String topic;
    private String workerType;
    private Integer partitionId;

    public ScKafkaOffsetId() {
    }

    public ScKafkaOffsetId(String topic, String workerType, Integer partitionId) {
        this.topic = topic;
        this.workerType = workerType;
        this.partitionId = partitionId;
    }

    public String getTopic() {
        return topic;
    }

    public String getWorkerType() {
        return workerType;
    }

    public Integer getPartitionId() {
        return partitionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScKafkaOffsetId that = (ScKafkaOffsetId) o;
        return Objects.equal(topic, that.topic) && Objects.equal(workerType, that.workerType) && Objects.equal(partitionId, that.partitionId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(topic, workerType, partitionId);
    }
}
