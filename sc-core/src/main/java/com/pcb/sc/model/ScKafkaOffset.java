package com.pcb.sc.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(ScKafkaOffsetId.class)
public class ScKafkaOffset {
    @Id private String topic;
    @Id private String workerType;
    @Id private Integer partitionId;
    private Long offsetId;

    public ScKafkaOffset() {
    }

    public ScKafkaOffset(Builder builder) {
        this.topic = builder.topic;
        this.workerType = builder.workerType;
        this.partitionId = builder.partitionId;
        this.offsetId = builder.offsetId;
    }

    public String getTopic() {
        return topic;
    }

    public String getWorkerType() {
        return workerType;
    }

    public Integer getPartitionId() {
        return partitionId;
    }

    public Long getOffsetId() {
        return offsetId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ScKafkaOffset{");
        sb.append("topic='").append(topic).append('\'');
        sb.append(", workerType='").append(workerType).append('\'');
        sb.append(", partitionId=").append(partitionId);
        sb.append(", offsetId=").append(offsetId);
        sb.append('}');
        return sb.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String topic;
        private String workerType;
        private Integer partitionId;
        private Long offsetId;

        private Builder() {
        }

        public Builder topic(String topic) {
            this.topic = topic;
            return this;
        }

        public Builder workerType(String workerType) {
            this.workerType = workerType;
            return this;
        }

        public Builder partitionId(Integer partitionId) {
            this.partitionId = partitionId;
            return this;
        }

        public Builder offsetId(Long offsetId) {
            this.offsetId = offsetId;
            return this;
        }

        public ScKafkaOffset build() {
            return new ScKafkaOffset(this);
        }
    }
}
