package com.pcb.sc.app.kafka.worker;

import com.pcb.commons.util.EnumHelper;

public enum WorkerType {

    TEST,
    CUSTOMER,
    CIS20,
    UNKNOWN;

    public static WorkerType fromString(String strValue) {
        return EnumHelper.fromString(WorkerType.class, strValue, UNKNOWN);
    }
}
