package com.pcb.sc.config;

import com.google.common.base.Preconditions;
import com.pcb.commons.util.config.ConfigLoader;
import com.pcb.kafkacore.common.util.EnumHelper;
import com.pcb.sc.app.kafka.worker.WorkerType;
import com.pcb.sc.service.OffsetDao;
import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Properties;
import java.util.function.BiFunction;

public abstract class WorkerConfig {

    private static final Logger logger = LoggerFactory.getLogger(WorkerConfig.class);

    private static final String KAFKA_PROPERTIES_FILE = "kafka.properties";
    private static final ConfigEntry<String> TOPIC_CONFIG_ENTRY = ConfigEntry.create("topic", null);
    private static final ConfigEntry<Long> FLUSH_INTERVAL_IN_MS_CONFIG_ENTRY = ConfigEntry.create("flushIntervalInMs", 30_000L);
    private static final ConfigEntry<Integer> MAX_NUMBER_OF_PROCESSED_RECORDS_IN_MEMORY_CONFIG_ENTRY = ConfigEntry.create("maxNumberOfProcessedRecordsInMemory", 200);
    private static final ConfigEntry<Long> POLL_TIMEOUT_IN_MS_CONFIG_ENTRY = ConfigEntry.create("pollTimeoutInMs", 1000L);
    private static final ConfigEntry<String> OFFSET_DAO_TYPE_CONFIG_ENTRY = ConfigEntry.create("offsetDao.type", "KAFKA");

    private final Configuration underlyingConfig;
    private final String workerId;
    private final WorkerType workerType;
    private final String namespace;

    private final String namespacedWorkerConfigKeyPrefix;
    private final String defaultWorkerConfigKeyPrefix;
    private final String topic;
    private final Properties kafkaProperties;

    public WorkerConfig(@Nonnull Configuration underlyingConfig, @Nonnull String workerId, @Nonnull WorkerType workerType, @Nonnull String namespace) {
        this.underlyingConfig = Preconditions.checkNotNull(underlyingConfig, "config cannot be null");
        this.workerId = Preconditions.checkNotNull(workerId, "workerId cannot be null");
        this.workerType = Preconditions.checkNotNull(workerType, "workerType cannot be null");

        this.namespace = Preconditions.checkNotNull(namespace, "namespace cannot be null");
        this.namespacedWorkerConfigKeyPrefix = getConfigKeyPrefix() + namespace + '.';
        this.defaultWorkerConfigKeyPrefix = getConfigKeyPrefix() + "default.";
        this.topic = Preconditions.checkNotNull(getString(TOPIC_CONFIG_ENTRY), "topic must be defined for workerId %s", workerId);
        this.kafkaProperties = loadKafkaProperties();
        this.kafkaProperties.setProperty("client.id", workerId);
    }

    private Properties loadKafkaProperties() {
        Properties kafkaProperties = ConfigLoader.getPropertiesFromFile(KAFKA_PROPERTIES_FILE);
        kafkaProperties.stringPropertyNames().forEach(propertyKey -> {
            if (propertyKey.startsWith("[")) {
                if (propertyKey.contains(namespace)) {
                    String newPropertyKey = propertyKey.substring(propertyKey.indexOf("]") + 1);
                    kafkaProperties.setProperty(newPropertyKey, kafkaProperties.getProperty(propertyKey));
                }

                kafkaProperties.remove(propertyKey);
            }
        });

        return kafkaProperties;
    }

    @Nonnull
    public Configuration underlyingConfig() {
        return underlyingConfig;
    }

    @Nonnull
    public String workerId() {
        return workerId;
    }

    @Nonnull
    public WorkerType workerType() {
        return workerType;
    }

    @Nonnull
    public String namespace() {
        return namespace;
    }

    @Nonnull
    public String topic() {
        return topic;
    }

    public long flushIntervalInMs() {
        return getLong(FLUSH_INTERVAL_IN_MS_CONFIG_ENTRY);
    }

    public int maxNumberOfProcessedRecordsInMemory() {
        return getInteger(MAX_NUMBER_OF_PROCESSED_RECORDS_IN_MEMORY_CONFIG_ENTRY);
    }

    public long pollTimeoutInMs() {
        return getLong(POLL_TIMEOUT_IN_MS_CONFIG_ENTRY);
    }

    @Nonnull
    public Properties kafkaProperties() {
        return kafkaProperties;
    }

    @Nonnull
    public OffsetDao.Impl offsetDaoType() {
        return EnumHelper.fromString(OffsetDao.Impl.class, getString(OFFSET_DAO_TYPE_CONFIG_ENTRY), OffsetDao.Impl.KAFKA);
    }

    @Nonnull
    protected abstract String getConfigKeyPrefix();

    protected Boolean getBoolean(ConfigEntry<Boolean> configEntry) {
        return getValue(underlyingConfig::getBoolean, configEntry.getKey(), configEntry.getDefaultValue());
    }

    protected Integer getInteger(ConfigEntry<Integer> configEntry) {
        return getValue(underlyingConfig::getInteger, configEntry.getKey(), configEntry.getDefaultValue());
    }

    protected Long getLong(ConfigEntry<Long> configEntry) {
        return getValue(underlyingConfig::getLong, configEntry.getKey(), configEntry.getDefaultValue());
    }

    protected Double getDouble(ConfigEntry<Double> configEntry) {
        return getValue(underlyingConfig::getDouble, configEntry.getKey(), configEntry.getDefaultValue());
    }

    protected String getString(ConfigEntry<String> configEntry) {
        return getValue(underlyingConfig::getString, configEntry.getKey(), configEntry.getDefaultValue());
    }

    private <T> T getValue(BiFunction<String, T, T> valueFunction, String configKey, T defaultValue) {
        String namespacedConfigKey = getNamespacedConfigKey(configKey);
        T namespacedValue = valueFunction.apply(namespacedConfigKey, null);
        if (namespacedValue != null) {
            return namespacedValue;
        } else {
            String defaultConfigKey = getDefaultConfigKey(configKey);
            return valueFunction.apply(defaultConfigKey, defaultValue);
        }
    }

    private String getNamespacedConfigKey(@Nonnull String configKey) {
        Preconditions.checkNotNull(configKey, "configKey cannot be null");
        return namespacedWorkerConfigKeyPrefix + configKey;
    }

    private String getDefaultConfigKey(@Nonnull String configKey) {
        Preconditions.checkNotNull(configKey, "configKey cannot be null");
        return defaultWorkerConfigKeyPrefix + configKey;
    }
}
