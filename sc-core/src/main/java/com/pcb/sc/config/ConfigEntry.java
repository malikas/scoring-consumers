package com.pcb.sc.config;

public interface ConfigEntry<T> {
    String getKey();

    T getDefaultValue();

    static <K> ConfigEntry<K> create(String key, K defaultValue) {
        return new SimpleConfigEntry<>(key, defaultValue);
    }
}
