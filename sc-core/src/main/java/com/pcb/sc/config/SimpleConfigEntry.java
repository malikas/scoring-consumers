package com.pcb.sc.config;

import java.util.Objects;

final class SimpleConfigEntry<T> implements ConfigEntry<T> {
    private final String key;
    private final T defaultValue;

    /**
     * @param key          name of the config property
     * @param defaultValue property default value in case the property is absent. Default can not be null.
     */
    SimpleConfigEntry(String key, T defaultValue) {
        Objects.requireNonNull(key, "key cannot be null");

        this.key = key;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public T getDefaultValue() {
        return defaultValue;
    }
}
