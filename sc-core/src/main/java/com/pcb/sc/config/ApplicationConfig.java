package com.pcb.sc.config;

import javax.annotation.Nonnull;
import java.util.List;

public interface ApplicationConfig {

    @Nonnull
    String applicationName();

    @Nonnull
    List<? extends WorkerConfig> workerConfigs();

}
