package com.pcb.sc.kafka.worker;

import com.pcb.sc.config.WorkerConfig;

import javax.annotation.Nonnull;

public interface WorkerFactory<K, V> {

    @Nonnull
    Worker<K, V> createWorker(@Nonnull WorkerConfig config);

}
