package com.pcb.sc.kafka.worker;

import com.google.common.base.Preconditions;
import com.pcb.kafkacore.common.serde.Deserializer;
import com.pcb.kafkacore.consumer.service.KafkaConsumer;
import com.pcb.kafkacore.consumer.service.impl.KafkaConsumerImpl;
import com.pcb.sc.config.WorkerConfig;
import com.pcb.sc.service.AllocationService;
import com.pcb.sc.service.BatchRecordProcessor;
import com.pcb.sc.service.OffsetDao;
import com.pcb.sc.service.OffsetManagementService;
import com.pcb.sc.service.impl.JpaOffsetDao;
import com.pcb.sc.service.impl.KafkaAllocationService;
import com.pcb.sc.service.impl.KafkaOffsetDao;
import com.pcb.sc.service.impl.OffsetManagementServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

public abstract class AbstractWorkerFactory<K, V> implements WorkerFactory<K, V> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractWorkerFactory.class);

    @Nonnull private final Deserializer<K> keyDeserializer;
    @Nonnull private final Deserializer<V> valueDeserializer;

    public AbstractWorkerFactory(@Nonnull Deserializer<K> keyDeserializer, @Nonnull Deserializer<V> valueDeserializer) {
        this.keyDeserializer = keyDeserializer;
        this.valueDeserializer = valueDeserializer;
    }

    @Nonnull
    @Override
    public Worker<K, V> createWorker(@Nonnull WorkerConfig workerConfig) {
        Preconditions.checkNotNull(workerConfig, "workerConfig cannot be null");
        KafkaConsumer<K, V> consumerAdapter = new KafkaConsumerImpl<>(workerConfig.workerId(), workerConfig.kafkaProperties(), keyDeserializer, valueDeserializer);
        OffsetDao offsetDao = offsetDao(workerConfig, consumerAdapter);
        AllocationService allocationService = new KafkaAllocationService();
        Worker<K, V> worker = new Worker<>(
                workerConfig,
                consumerAdapter,
                allocationService,
                offsetManagementService(workerConfig, offsetDao),
                recordProcessor(workerConfig)
        );

        logger.info("Worker {} has been created for topic: {}", workerConfig.workerId(), workerConfig.topic());
        return worker;
    }

    private OffsetDao offsetDao(@Nonnull WorkerConfig workerConfig, @Nonnull KafkaConsumer<?, ?> consumer) {
        switch (workerConfig.offsetDaoType()) {
            case KAFKA:
                return new KafkaOffsetDao(consumer);
            case JDBC:
                return new JpaOffsetDao(workerConfig.workerType());
            default:
                throw new UnsupportedOperationException("Unsupported offsetDao type: " + workerConfig.offsetDaoType());
        }
    }

    private OffsetManagementService offsetManagementService(@Nonnull WorkerConfig workerConfig, @Nonnull OffsetDao offsetDao) {
        return new OffsetManagementServiceImpl(workerConfig.topic(), offsetDao);
    }

    protected abstract BatchRecordProcessor<K, V> recordProcessor(@Nonnull WorkerConfig workerConfig);
}
