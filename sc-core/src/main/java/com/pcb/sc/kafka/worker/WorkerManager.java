package com.pcb.sc.kafka.worker;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Manages the starting and stopping of individual consumer workers.
 */
public interface WorkerManager {

    /**
     * Returns a map of workers indexed by their respective IDs.
     *
     * @return A map of workers indexed by their respective IDs
     */
    @Nonnull
    Map<String, Worker> workers();

    void start();

    void shutdown();

}
