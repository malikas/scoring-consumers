package com.pcb.sc.kafka.worker;

import com.google.common.base.Preconditions;
import com.pcb.kafkacore.consumer.model.ConsumerRebalanceListener;
import com.pcb.kafkacore.consumer.model.TopicPartition;
import com.pcb.sc.service.AllocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class RebalanceListener implements ConsumerRebalanceListener {

    private static final Logger logger = LoggerFactory.getLogger(RebalanceListener.class);

    private final String workerId;
    private final AllocationService allocationService;

    RebalanceListener(@Nonnull String workerId, @Nonnull AllocationService allocationService) {
        this.workerId = Preconditions.checkNotNull(workerId, "workerId cannot be null");
        this.allocationService = Preconditions.checkNotNull(allocationService, "allocationService cannot be null");
    }

    @Override
    public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        Set<Integer> revokedPartitions = partitions.stream()
                .map(TopicPartition::partition)
                .collect(Collectors.toSet());

        allocationService.revokePartitions(revokedPartitions);
        logger.info("Partitions revoked for worker {}: {}", workerId, partitions);
    }

    @Override
    public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
        Set<Integer> assignedPartitions = partitions.stream()
                .map(TopicPartition::partition)
                .collect(Collectors.toSet());

        allocationService.assignPartitions(assignedPartitions);
        logger.info("Partitions assigned for worker {}: {}", workerId, partitions);
    }
}
