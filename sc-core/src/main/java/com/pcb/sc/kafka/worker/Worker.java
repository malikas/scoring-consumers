package com.pcb.sc.kafka.worker;

import com.google.common.base.Preconditions;
import com.pcb.commons.core.persistence.JpaPersistenceConnection;
import com.pcb.kafkacore.consumer.model.ConsumerRecord;
import com.pcb.kafkacore.consumer.model.ConsumerRecords;
import com.pcb.kafkacore.consumer.model.TopicPartition;
import com.pcb.kafkacore.consumer.service.KafkaConsumer;
import com.pcb.sc.config.WorkerConfig;
import com.pcb.sc.service.AllocationService;
import com.pcb.sc.service.BatchRecordProcessor;
import com.pcb.sc.service.OffsetDao;
import com.pcb.sc.service.OffsetManagementService;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Worker thread which, from a high-level, executes the following series of operations:
 * <p>
 * 1) Poll messages from Kafka.
 * 2) Process messages.
 * 3) Commit Kafka offsets.
 * <p>
 * It is guaranteed to provide "at least once" message processing guarantee as it always does its best to process
 * the messages first before committing the offsets to the underlying data store. As a result, the messages can appear
 * twice in the final sink destination.
 *
 * @param <K> Kafka message key type
 * @param <V> Kafka message value type
 */
public class Worker<K, V> implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Worker.class);

    private static final Either<ResetReason, Void> SUCCESS = Either.right(null);

    public enum ResetReason {
        INITIAL_RESET,
        RESET_FAILURE,
        POLL_FAILURE,
        FLUSH_FAILURE,
        ALLOCATIONS_CHANGED,
        COMMIT_FAILURE,
        PROCESSING_DELAY_REQUIRED
    }

    public enum FlushReason {
        FLUSH_INTERVAL_EXCEEDED,
        TOO_MANY_RECORDS_IN_MEMORY,
        FLUSH_ON_RESET,
        SHUTDOWN
    }

    @Nonnull private final WorkerConfig config;
    @Nonnull private final KafkaConsumer<K, V> consumer;
    @Nonnull private final AllocationService allocationService;
    @Nonnull private final OffsetManagementService offsetManagementService;
    @Nonnull private final BatchRecordProcessor<K, V> recordProcessor;

    @Nonnull private final AtomicReference<ResetReason> resetRequired = new AtomicReference<>();
    @Nonnull private final CountDownLatch runningLatch = new CountDownLatch(1);
    @Nonnull private final AtomicBoolean shutdownRequested = new AtomicBoolean();

    private int recordsProcessedInMemory;
    private long lastFlushTime = DateTimeUtils.currentTimeMillis();

    public Worker(@Nonnull WorkerConfig config,
                  @Nonnull KafkaConsumer<K, V> consumer,
                  @Nonnull AllocationService allocationService,
                  @Nonnull OffsetManagementService offsetManagementService,
                  @Nonnull BatchRecordProcessor<K, V> recordProcessor) {
        this.config = Preconditions.checkNotNull(config, "config cannot be null");
        this.consumer = Preconditions.checkNotNull(consumer, "consumer cannot be null");
        this.allocationService = Preconditions.checkNotNull(allocationService, "allocationService cannot be null");
        this.offsetManagementService = Preconditions.checkNotNull(offsetManagementService, "offsetManagementService cannot be null");
        this.recordProcessor = Preconditions.checkNotNull(recordProcessor, "recordProcessor cannot be null");
    }

    @Nonnull
    public WorkerConfig getConfig() {
        return config;
    }

    @Nonnull
    public Set<Integer> getAssignedPartitions() {
        return allocationService.getAssignedPartitions();
    }

    @Override
    public void run() {
        try {
            subscribe();
            initialize();

            resetRequired.set(ResetReason.INITIAL_RESET);

            while (!shutdownRequested.get() && !Thread.currentThread().isInterrupted()) {
                resetIfRequired()
                        .flatMap(success -> pollRecords())
                        .flatMap(this::batchRecords)
                        .flatMap(success -> flushIfRequired())
                        .orElseRun(resetRequired::set);
            }

            // Last flush before complete shutdown
            flushIfRequired(FlushReason.SHUTDOWN);
        } catch (RuntimeException e) {
            throw e;
        } finally {
            logger.info("Shutting down consumer");
            consumer.shutdown();
            runningLatch.countDown();
        }
    }

    private void subscribe() {
        consumer.subscribe(Collections.singletonList(config.topic()), new RebalanceListener(config.workerId(), allocationService));
    }

    private void initialize() {
        Try.of(consumer::immediatePoll).onFailure(e -> logger.warn("Initial poll failed: ", e));

        logger.info("Waiting for initial Kafka partition allocations");
        allocationService.awaitInitialAllocation();
        allocationService.hasChangeFiredAndReset();

        logger.info("Initialization complete!");
    }

    private Either<ResetReason, ConsumerRecords<K, V>> pollRecords() {
        try {
            ConsumerRecords<K, V> records = consumer.poll(config.pollTimeoutInMs());
            return checkAllocationReset().map(success -> records);
        } catch (RuntimeException e) {
            logger.error("Error polling records", e);
            return Either.left(ResetReason.POLL_FAILURE);
        }
    }

    private Either<ResetReason, Void> checkAllocationReset() {
        boolean allocationHasChanged = allocationService.hasChangeFiredAndReset();
        return allocationHasChanged ? Either.left(ResetReason.ALLOCATIONS_CHANGED) : SUCCESS;
    }

    private Either<ResetReason, Void> batchRecords(@Nonnull ConsumerRecords<K, V> records) {
        for (ConsumerRecord<K, V> record : records) {
            offsetManagementService.markProcessed(record.partition(), record.offset());
            logger.trace("Processing {}-{}-{}", record.topic(), record.partition(), record.offset());
            recordProcessor.add(record);

            Either<ResetReason, Void> flushResult = flushIfRequired();
            if (!SUCCESS.eq(flushResult)) {
                return flushResult;
            }
        }

        recordsProcessedInMemory += records.count();
        return SUCCESS;
    }

    private Either<ResetReason, Void> resetIfRequired() {
        ResetReason resetReason = resetRequired.getAndSet(null);
        return resetReason == null ? SUCCESS : reset(resetReason);
    }

    private Either<ResetReason, Void> reset(ResetReason resetReason) {
        Preconditions.checkNotNull(resetReason, "resetReason cannot be null");

        flush(FlushReason.FLUSH_ON_RESET);
        try {
            recordProcessor.clear();
            Map<TopicPartition, Long> offsetsByPartition = offsetManagementService.reset();
            logger.debug("Resetting offsets to {} for {}", offsetsByPartition, config.workerId());
            consumer.seek(offsetsByPartition);

            recordsProcessedInMemory = 0;
            resetRequired.set(null);
            return SUCCESS;
        } catch (RuntimeException e) {
            logger.error("Error resetting consumer", e);
            return Either.left(ResetReason.RESET_FAILURE);
        }
    }

    private Either<ResetReason, Void> flushIfRequired() {
        return flushIfRequired(null);
    }

    private Either<ResetReason, Void> flushIfRequired(@Nullable FlushReason flushReason) {
        if (flushReason == null) {
            final long lastFlushTime = this.lastFlushTime;
            if (DateTimeUtils.currentTimeMillis() - lastFlushTime >= config.flushIntervalInMs()) {
                return flush(FlushReason.FLUSH_INTERVAL_EXCEEDED);
            } else if (recordsProcessedInMemory >= config.maxNumberOfProcessedRecordsInMemory()) {
                return flush(FlushReason.TOO_MANY_RECORDS_IN_MEMORY);
            } else {
                return SUCCESS;
            }
        } else {
            return flush(flushReason);
        }
    }

    @Nonnull
    private Either<ResetReason, Void> flush(@Nonnull FlushReason flushReason) {
        Preconditions.checkNotNull(flushReason, "flushReason cannot be null");

        Optional<BatchRecordProcessor.FlushFailureReason> failureReason = recordProcessor.flush();

        if (failureReason.isPresent()) {
            switch (failureReason.get()) {
                case PROCESSING_DELAY_REQUIRED:
                    return Either.left(ResetReason.PROCESSING_DELAY_REQUIRED);
                default:
                    return Either.left(ResetReason.FLUSH_FAILURE);
            }
        } else {
            lastFlushTime = DateTimeUtils.currentTimeMillis();
            try {
                offsetManagementService.commit();
            } catch (RuntimeException e) {
                return Either.left(ResetReason.COMMIT_FAILURE);
            }

            recordsProcessedInMemory = 0;
            return SUCCESS;
        }
    }

    public void shutdown() {
        if (shutdownRequested.getAndSet(true)) {
            logger.warn("Shutdown has been already requested for {}. Skipping this attempt.", config.workerId());
            return;
        }

        try {
            logger.info("Waiting for run() method to complete for {}", config.workerId());
            runningLatch.await();
        } catch (InterruptedException e) {
            logger.error("runningLatch for {} was interrupted", config.workerId(), e);
            Thread.currentThread().interrupt();
        }

        logger.info("Waiting for {} recordProcessor.shutdown() to complete", config.workerId());
        recordProcessor.shutdown();

        if (config.offsetDaoType() == OffsetDao.Impl.JDBC) {
            JpaPersistenceConnection.INSTANCE.shutdown();
        }
    }
}
