package com.pcb.sc.kafka.worker;

import com.google.common.base.Preconditions;
import com.pcb.sc.app.kafka.worker.WorkerType;
import com.pcb.sc.config.ApplicationConfig;
import com.pcb.sc.config.WorkerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class DefaultWorkerManager implements WorkerManager {

    private static final Logger logger = LoggerFactory.getLogger(DefaultWorkerManager.class);

    private final ApplicationConfig appConfig;
    private final Map<WorkerType, WorkerFactory<?, ?>> workerFactories;

    private final AtomicBoolean shutdownRequested;
    private Map<String, Worker<?, ?>> workerMap;

    public DefaultWorkerManager(ApplicationConfig appConfig, Map<WorkerType, WorkerFactory<?, ?>> workerFactories) {
        this.appConfig = Preconditions.checkNotNull(appConfig, "appConfig cannot be null");
        this.workerFactories = Preconditions.checkNotNull(workerFactories, "workerFactories cannot be null");

        this.shutdownRequested = new AtomicBoolean();
    }

    @Override
    public void start() {
        logger.info("Creating workers");
        this.workerMap = appConfig.workerConfigs().stream().collect(Collectors.toMap(WorkerConfig::workerId, config -> {
            WorkerFactory<?, ?> factory = workerFactories.get(config.workerType());
            Preconditions.checkNotNull(factory, "workerFactory was not found for worker: %s", config.workerId());
            return factory.createWorker(config);
        }));

        logger.info("Starting workers");
        workerMap.forEach((workerId, worker) ->
            new Thread(() -> {
                try {
                    worker.run();
                    logger.info("Worker [{}] terminated gracefully", workerId);
                } catch (RuntimeException e) {
                    logger.error("Worker [{}] died", workerId, e);
                }
            }, "worker-" + workerId).start());
    }

    @Override
    public void shutdown() {
        if (shutdownRequested.getAndSet(true)) {
            logger.warn("Shutdown has been already requested. Skipping this attempt");
            return;
        }

        if (workerMap != null) {
            workerMap.forEach((id, worker) -> worker.shutdown());
        }
    }

    @Nonnull
    @Override
    public Map<String, Worker> workers() {
        if (workerMap == null) {
            return Collections.emptyMap();
        }

        return Collections.unmodifiableMap(workerMap);
    }
}
