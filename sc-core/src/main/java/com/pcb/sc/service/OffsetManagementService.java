package com.pcb.sc.service;

import com.pcb.kafkacore.consumer.model.TopicPartition;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Provides mechanism to retrieve and update offsets for a single topic
 */
public interface OffsetManagementService {

    /**
     * Synchronizes offsets with the underlying data store, discarding any uncommitted offsets in the process. Note that
     * this method only returns the next offsets for the assigned partitions.
     *
     * @return The next offsets to start processing for the assigned partitions
     * @throws RuntimeException If an unexpected error occurred while trying to synchronize offsets
     */
    Map<TopicPartition, Long> reset() throws RuntimeException;

    /**
     * Marks the specified offset for the given partition to have been processed. The updated offset is not saved to
     * any underlying data store until {@link #commit()} is called.
     *
     * @param partition Partition which the updated offset corresponds to
     * @param offset Offset of the last processed message for the given partition
     */
    void markProcessed(int partition, long offset);

    /**
     * Commits all offsets that have been marked as processed to a persistent underlying data store.
     *
     * @throws RuntimeException If an unexpected error occurred while trying to commit the offsets
     */
    void commit() throws RuntimeException;

    /**
     * Returns the current offset (i.e. index of the last processed message) for each partition. This takes into account
     * both committed and uncommitted offsets.
     *
     * @return The current offsets indexed by partition
     */
    @Nonnull
    Map<Integer, Long> getProcessedOffsets();

}
