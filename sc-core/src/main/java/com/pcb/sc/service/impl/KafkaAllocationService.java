package com.pcb.sc.service.impl;

import com.pcb.sc.service.AllocationService;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

public class KafkaAllocationService implements AllocationService {

    private final Set<Integer> assignedPartitions;
    private final CountDownLatch initialAllocationLatch;
    private volatile boolean hasChangedSinceLastAccess;

    public KafkaAllocationService() {
        this.assignedPartitions = new HashSet<>();
        this.initialAllocationLatch = new CountDownLatch(1);
        this.hasChangedSinceLastAccess = false;
    }

    @Nonnull
    @Override
    public Set<Integer> getAssignedPartitions() {
        return Collections.unmodifiableSet(assignedPartitions);
    }

    @Override
    public void assignPartitions(Set<Integer> assignedPartitions) {
        boolean modified = this.assignedPartitions.addAll(assignedPartitions);
        if (modified) {
            hasChangedSinceLastAccess = true;
        }

        initialAllocationLatch.countDown();
    }

    @Override
    public void revokePartitions(Set<Integer> revokedPartitions) {
        boolean modified = this.assignedPartitions.removeAll(revokedPartitions);
        if (modified) {
            hasChangedSinceLastAccess = true;
        }
    }

    @Override
    public boolean hasChangeFiredAndReset() {
        boolean previousState = hasChangedSinceLastAccess;
        hasChangedSinceLastAccess = false;
        return previousState;
    }

    @Override
    public void awaitInitialAllocation() {
        try {
            initialAllocationLatch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
