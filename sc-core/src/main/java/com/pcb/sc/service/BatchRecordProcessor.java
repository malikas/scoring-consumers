package com.pcb.sc.service;

import com.pcb.kafkacore.consumer.model.ConsumerRecord;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Provides mechanisms for collecting consumed Kafka records, and batch processing them later on.
 *
 * @param <K> Kafka record key type
 * @param <V> Kafka record value (i.e. body) type
 */
public interface BatchRecordProcessor<K, V> {

    enum AddFailureReason {
        BAD_RECORD                 // Record failed to validate.
    }

    enum FlushFailureReason {
        PROCESSING_DELAY_REQUIRED, // Records in the current poll require delayed processing -- i.e. it's too soon to process the batch.
        UNEXPECTED_ERROR           // An unexpected error occurred.
    }

    /**
     * Adds a single record to the batch to be processed later on.
     *
     * @param record Record to add to the batch
     * @return If the record failed to be validated, a reject reason will be provided
     */
    @Nonnull
    Optional<AddFailureReason> add(ConsumerRecord<K, V> record);

    /**
     * Processes all messages in the batch and, if successful, clears the batch.
     *
     * @return If the batched messages couldn't or shouldn't be processed yet, a failure reason is returned
     */
    @Nonnull
    Optional<FlushFailureReason> flush();

    /**
     * Clears all records in the batch without processing them.
     */
    void clear();

    /**
     * Returns the number of pending messages in the current batch.
     *
     * @return The number of pending messages in the current batch
     */
    int getBatchSize();

    void shutdown();
}
