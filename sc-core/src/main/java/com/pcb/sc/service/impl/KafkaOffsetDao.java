package com.pcb.sc.service.impl;

import com.google.common.base.Preconditions;
import com.pcb.kafkacore.consumer.service.KafkaConsumer;
import com.pcb.sc.service.OffsetDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * OffsetDao implementation that stores Kafka offsets in Kafka.
 */
public class KafkaOffsetDao implements OffsetDao {

    private static final Logger logger = LoggerFactory.getLogger(KafkaOffsetDao.class);

    @Nonnull private final KafkaConsumer<?, ?> consumer;

    public KafkaOffsetDao(@Nonnull KafkaConsumer<?, ?> consumer) {
        this.consumer = Preconditions.checkNotNull(consumer, "consumer cannot be null");
    }

    @Nonnull
    @Override
    public Map<Integer, Long> getOffsetsByPartition(@Nonnull String topic) throws RuntimeException {
        logger.debug("Retrieving offsets for topic {}", topic);
        return consumer.getLastCommittedOffsetsForAssignedPartitions(topic);
    }

    @Override
    public void updateOffsets(@Nonnull String topic, @Nonnull Map<Integer, Long> offsetsByPartition) throws RuntimeException {
        logger.debug("Updating offsets for topic {} to: {}", topic, offsetsByPartition);
        consumer.commit(topic, offsetsByPartition);
    }
}
