package com.pcb.sc.service;

import javax.annotation.Nonnull;
import java.util.Set;

public interface AllocationService {

    @Nonnull
    Set<Integer> getAssignedPartitions();

    void assignPartitions(Set<Integer> partitions);

    void revokePartitions(Set<Integer> partitions);

    boolean hasChangeFiredAndReset();

    void awaitInitialAllocation();

}
