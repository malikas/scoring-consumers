package com.pcb.sc.service.impl;

import com.google.common.base.Preconditions;
import com.pcb.commons.core.persistence.Datastore;
import com.pcb.commons.core.persistence.JpaPersistenceConnection;
import com.pcb.sc.app.kafka.worker.WorkerType;
import com.pcb.sc.model.ScKafkaOffset;
import com.pcb.sc.service.OffsetDao;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OffsetDao implementation that stores Kafka offsets in the underlying RDBMS. The actual implementation is based
 * on the JPA provider, e.g., Hibernate.
 */
public class JpaOffsetDao implements OffsetDao {

    private static final Logger logger = LoggerFactory.getLogger(JpaOffsetDao.class);

    @Nonnull
    private final WorkerType workerType;
    @Nonnull
    private final Class<?>[] classes;

    public JpaOffsetDao(@Nonnull WorkerType workerType) {
        this.workerType = Preconditions.checkNotNull(workerType, "workerType cannot be null");
        this.classes = new Class[]{ScKafkaOffset.class};
    }

    @NotNull
    @Override
    public Map<Integer, Long> getOffsetsByPartition(@NotNull String topic) throws RuntimeException {
        logger.debug("Retrieving offsets for topic {}", topic);

        EntityManager entityManager = null;
        Map<Integer, Long> offsetsByPartition = new HashMap<>();

        try {
            entityManager = JpaPersistenceConnection.getEntityManager(Datastore.ScKafkaOffsetDB, classes);

            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<ScKafkaOffset> criteriaQuery = criteriaBuilder.createQuery(ScKafkaOffset.class);
            Root<ScKafkaOffset> root = criteriaQuery.from(ScKafkaOffset.class);
            criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("topic"), topic));

            List<ScKafkaOffset> offsetsByPartitionList = entityManager.createQuery(criteriaQuery).getResultList();

            for (ScKafkaOffset kafkaOffset : offsetsByPartitionList) {
                Long existing = offsetsByPartition.putIfAbsent(kafkaOffset.getPartitionId(), kafkaOffset.getOffsetId());
                if (existing != null && existing > kafkaOffset.getOffsetId()) {
                    offsetsByPartition.put(kafkaOffset.getPartitionId(), kafkaOffset.getOffsetId());
                }
            }
        } catch (Exception e) {
            logger.error("Failed to retrieve offsets for topic {} and worker type {}", topic, workerType);
            throw new RuntimeException(e);
        } finally {
            if (entityManager != null)
                entityManager.close();
        }

        return offsetsByPartition;
    }

    @Override
    public void updateOffsets(@NotNull String topic, @NotNull Map<Integer, Long> offsetsByPartition) throws RuntimeException {
        logger.debug("Updating offsets for topic {} to: {}", topic, offsetsByPartition);

        if (offsetsByPartition.size() == 0) {
            return;
        }

        EntityManager entityManager = null;

        try {
            entityManager = JpaPersistenceConnection.getEntityManager(Datastore.ScKafkaOffsetDB, classes);

            ScKafkaOffset.Builder scKafkaOffset = ScKafkaOffset.builder()
                    .topic(topic)
                    .workerType(workerType.name());

            entityManager.getTransaction().begin();

            for (Map.Entry<Integer, Long> offsetForTopicPartition : offsetsByPartition.entrySet()) {
                ScKafkaOffset kafkaOffset = scKafkaOffset
                        .partitionId(offsetForTopicPartition.getKey())
                        .offsetId(offsetForTopicPartition.getValue())
                        .build();

                entityManager.merge(kafkaOffset);
            }

            entityManager.getTransaction().commit();
        } catch (Exception e) {
            if (entityManager != null)
                entityManager.getTransaction().rollback();
            logger.error("Failed to persist offsets {} for topic {} and worker type {}", offsetsByPartition, topic, workerType);
            throw new RuntimeException(e);
        } finally {
            if (entityManager != null)
                entityManager.close();
        }
    }

}
