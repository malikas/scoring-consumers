package com.pcb.sc.service.impl;

import com.google.common.base.Preconditions;
import com.pcb.kafkacore.consumer.model.TopicPartition;
import com.pcb.sc.service.OffsetDao;
import com.pcb.sc.service.OffsetManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class OffsetManagementServiceImpl implements OffsetManagementService {

    private static final Logger logger = LoggerFactory.getLogger(OffsetManagementServiceImpl.class);

    @Nonnull private final String topic;
    @Nonnull private final OffsetDao offsetDao;

    @Nonnull private final Map<Integer, Long> committedOffsetsByPartition = new HashMap<>();
    @Nonnull private final Map<Integer, Long> pendingOffsetsByPartition = new HashMap<>();

    public OffsetManagementServiceImpl(@Nonnull String topic, @Nonnull OffsetDao offsetDao) {
        this.topic = Preconditions.checkNotNull(topic, "topic cannot be null");
        this.offsetDao = Preconditions.checkNotNull(offsetDao, "offsetDao cannot be null");
    }

    @Override
    public Map<TopicPartition, Long> reset() throws RuntimeException {
        committedOffsetsByPartition.clear();
        pendingOffsetsByPartition.clear();

        Map<Integer, Long> nextOffsetsByPartition = offsetDao.getOffsetsByPartition(topic);
        nextOffsetsByPartition.forEach((partition, offset) -> committedOffsetsByPartition.put(partition, offset - 1));
        return nextOffsetsByPartition.entrySet().stream().collect(Collectors.toMap(e -> new TopicPartition(topic, e.getKey()), Map.Entry::getValue));
    }

    @Override
    public void markProcessed(int partition, long newOffset) {
        // If we've already moved past the given offset, we don't have to do anything
        Long pendingOffset = pendingOffsetsByPartition.get(partition);
        if (pendingOffset != null && pendingOffset >= newOffset)
            return;
        Long committedOffset = committedOffsetsByPartition.get(partition);
        if (committedOffset != null && committedOffset >= newOffset)
            return;

        // Normally, the new offset is the previous offset +1; if anything seems unusual, log a warning
        Long previousOffset = pendingOffset == null ? committedOffset : pendingOffset;
        if (previousOffset == null && newOffset != 0) {
            logger.warn("No previous offset was found for partition {}-{}; adding pending offset of {}", topic, partition, newOffset);
        }
        if (previousOffset != null && previousOffset + 1 != newOffset) {
            logger.warn("Skipped records {} to {} on {}-{} (pending={})", previousOffset, newOffset, topic, partition, pendingOffset);
        }

        pendingOffsetsByPartition.put(partition, newOffset);
    }

    @Override
    public void commit() throws RuntimeException {
        // Persist the offsets of the next messages to consume
        Map<Integer, Long> nextOffsetByPartition = pendingOffsetsByPartition.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue() + 1));
        offsetDao.updateOffsets(topic, nextOffsetByPartition);

        // Roll all pending offsets to the map of committed offsets
        committedOffsetsByPartition.putAll(pendingOffsetsByPartition);
        pendingOffsetsByPartition.clear();
    }

    @Nonnull
    @Override
    public Map<Integer, Long> getProcessedOffsets() {
        Map<Integer, Long> effectiveOffsets = new HashMap<>(committedOffsetsByPartition);
        effectiveOffsets.putAll(pendingOffsetsByPartition);
        return effectiveOffsets;
    }
}
