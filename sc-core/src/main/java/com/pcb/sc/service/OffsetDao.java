package com.pcb.sc.service;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * DAO for saving and retrieving Kafka offsets from an underlying persistent data store.
 */
public interface OffsetDao {

    enum Impl {
        KAFKA,
        JDBC,
        IN_MEMORY
    }

    /**
     * Retrieves the offsets of the next unconsumed message for each partition of a given topic.
     *
     * @param topic Topic whose offsets to retrieve
     * @return The retrieved offsets
     * @throws RuntimeException If the offsets could not be retrieved for any reason
     */
    @Nonnull
    Map<Integer, Long> getOffsetsByPartition(@Nonnull String topic) throws RuntimeException;

    /**
     * Saves the offsets of the next unconsumed message for the specified partitions of a given topic.
     *
     * @param topic              Topic whose offsets to save
     * @param offsetsByPartition Offsets to save
     * @throws RuntimeException If the offsets failed to update for any reason
     */
    void updateOffsets(@Nonnull String topic, @Nonnull Map<Integer, Long> offsetsByPartition) throws RuntimeException;

}
