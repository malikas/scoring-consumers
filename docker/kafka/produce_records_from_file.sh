#!/bin/bash

function usage() {
  cat <<EOM
DESCRIPTION:

  Produces the records to a given topic from a file.

USAGE: $0 topicName fileName

  topicName   - topic name you want to produce to
  fileName    - file name where the records are stored

EOM
  exit 1
}

if [[ $# != 2 ]]; then usage; fi

TOPIC_NAME=$1
FILE_NAME=$2

docker exec kafkacat kafkacat -b kafka:29092 -P -t $TOPIC_NAME -T -l $FILE_NAME