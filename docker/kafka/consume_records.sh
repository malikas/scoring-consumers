#!/bin/bash

function usage() {
  cat <<EOM
DESCRIPTION:

  Consumes the records from a given topic (by default, the consumer starts consuming from beginning)

USAGE: $0 topicName

  topicName   - topic name you want to consume from

EOM
  exit 1
}

if [[ $# == 0 ]]; then usage; fi

TOPIC_NAME=$1

docker exec kafkacat kafkacat -b kafka:29092 -C -t $TOPIC_NAME -o beginning