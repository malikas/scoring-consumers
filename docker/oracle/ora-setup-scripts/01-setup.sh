#!/bin/sh

echo 'Configuring Oracle'

# Set archive log mode and enable GG replication
ORACLE_SID=ORCLCDB
export ORACLE_SID
sqlplus /nolog <<- EOF
	CONNECT sys/Admin123 AS SYSDBA
	alter system set db_recovery_file_dest_size = 5G;
	alter system set db_recovery_file_dest = '/opt/oracle/oradata/recovery_area' scope=spfile;
	shutdown immediate
	startup mount
	alter database archivelog;
	alter database open;
        -- Should show "Database log mode: Archive Mode"
	archive log list
	exit;
EOF

# Create test user
sqlplus sys/Admin123@//localhost:1521/ORCLPDB1 as sysdba <<- EOF
	CREATE USER test_user IDENTIFIED BY test;
	GRANT CONNECT TO test_user;
	GRANT CREATE SESSION TO test_user;
	GRANT CREATE TABLE TO test_user;
	GRANT CREATE SEQUENCE TO test_user;
	GRANT CREATE TRIGGER TO test_user;
	ALTER USER test_user QUOTA 100M ON users;

	exit;
EOF