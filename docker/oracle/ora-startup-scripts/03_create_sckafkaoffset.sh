#!/bin/sh

echo 'Creating test_user.sckafkaoffset table'

sqlplus test_user/test@//localhost:1521/ORCLPDB1  <<- EOF

  drop table ScKafkaOffset purge;

  create table ScKafkaOffset (
          topic VARCHAR(100) DEFAULT ('') NOT NULL ,
          workerType VARCHAR(50) DEFAULT ('') NOT NULL,
          partitionId NUMBER(10) DEFAULT ('0') NOT NULL,
          offsetId NUMBER(19) DEFAULT (NULL),
          CONSTRAINT pk_sckafkaoffset PRIMARY KEY(topic, workerType, partitionId)
  );

EOF