#!/bin/bash

curl -i -X PUT -H "Accept:application/json" \
    -H  "Content-Type:application/json" http://localhost:8083/connectors/source-oracle-jdbc/config/ \
    -d '{
            "connector.class": "io.confluent.connect.jdbc.JdbcSourceConnector",
            "connection.url": "jdbc:oracle:thin:@oracle:1521/ORCLPDB1",
            "connection.user":"test_user",
            "connection.password":"test",
            "numeric.mapping":"best_fit",
            "mode":"timestamp",
            "poll.interval.ms":"1000",
            "validate.non.null":"false",
            "table.whitelist":"CUSTOMERS",
            "timestamp.column.name":"UPDATE_TS",
            "topic.prefix":"ora-",
            "key.converter": "org.apache.kafka.connect.storage.StringConverter",
            "value.converter": "io.confluent.connect.json.JsonSchemaConverter",
            "value.converter.schema.registry.url": "http://schema-registry:8081"
        }'