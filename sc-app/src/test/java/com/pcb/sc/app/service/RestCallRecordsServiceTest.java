package com.pcb.sc.app.service;

import com.pcb.sc.app.model.Cis20Record;
import com.pcb.sc.app.parser.impl.Cis20RecordParser;
import com.pcb.sc.app.service.impl.RestCallRecordsService;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;

public class RestCallRecordsServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(RestCallRecordsServiceTest.class);

    private MockWebServer server;
    private Configuration configuration;
    private RestCallRecordsService<String, Cis20Record> restCallRecordsServiceCis20;

    private String cis20RecordStr;

    @Before
    public void setUp() throws IOException {
        server = new MockWebServer();
        server.enqueue(new MockResponse().setResponseCode(200));
        server.start();

        configuration = new PropertiesConfiguration();
        configuration.setProperty("RestCallRecordsService.scoringServerUrl", server.url("/FalconRestApi/"));
        configuration.setProperty("RestCallRecordsService.fileErrorPath", "./");
        configuration.setProperty("RestCallRecordsService.fileErrorName", "scoringServerFailedRequests.txt");

        restCallRecordsServiceCis20 = new RestCallRecordsService<>(configuration, new Cis20RecordParser());

        cis20RecordStr = "DmodelSTUB       CIS20   2.0  PCF             20201214212549915      0000009911396                                               CIS00000000000000000000000000001IS2016020912   liam                          HAXDQNSEXPHTDLNXTMBG          AEXGJOTGZMUMAOUUXKDT                                        Other                                                                           en                                                                                                                         23 Henry O' Way                                                                                                                                                 Chatham                                 ON N7L5M6    124                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      RET                4               124             7207977550              5838934236                                                              HVineesha.Paritala@mycomp.ca              19510101                      124                                                                     544 20201214EQUIFAX             RPOORN                                 YYN                                                                                                                                                                                                                                                                                                                               ";
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }

    @Test
    public void testCis20Success() throws InterruptedException {
        server.url("/FalconRestApi/CIS20/");
        server.setDispatcher(new Dispatcher() {
            @NotNull
            @Override
            public MockResponse dispatch(@NotNull RecordedRequest recordedRequest) {
                return new MockResponse().setResponseCode(200);
            }
        });

        restCallRecordsServiceCis20.processRecords(Collections.singletonList(cis20RecordStr), false);
        RecordedRequest request = server.takeRequest();

        logger.info("Request Body: {}", request.getBody().readUtf8());
        Assert.assertEquals("application/json; charset=UTF-8", request.getHeaders().get("Content-Type"));
    }

    @Test
    public void testCis20Failure() {
        server.url("/FalconRestApi/CIS20/");
        server.setDispatcher(new Dispatcher() {
            @NotNull
            @Override
            public MockResponse dispatch(@NotNull RecordedRequest recordedRequest) {
                return new MockResponse().setResponseCode(400);
            }
        });

        restCallRecordsServiceCis20.processRecords(Collections.singletonList(cis20RecordStr), false);
    }
}
