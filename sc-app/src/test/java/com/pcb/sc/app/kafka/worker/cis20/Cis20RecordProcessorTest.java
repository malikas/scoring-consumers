package com.pcb.sc.app.kafka.worker.cis20;

import com.pcb.kafkacore.consumer.model.ConsumerRecord;
import com.pcb.sc.app.service.RecordsService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class Cis20RecordProcessorTest {

    @Mock private Cis20WorkerConfig workerConfig;
    @Mock private RecordsService<String> batchFileRecordsService;
    @Mock private RecordsService<String> restCallRecordsService;

    private Cis20RecordProcessor recordProcessor;

    private static ConsumerRecord<String, String> record1, record2, record3;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        record1 = new TestConsumerRecord<>("myTopic", 0, UUID.randomUUID().toString(), "Test value", 0);
        record2 = new TestConsumerRecord<>("myTopic", 0, UUID.randomUUID().toString(), "Test value", 1);
        record3 = new TestConsumerRecord<>("myTopic", 0, UUID.randomUUID().toString(), "Test value", 2);

        recordProcessor = new Cis20RecordProcessor(workerConfig, batchFileRecordsService, restCallRecordsService);
        assertEquals(0, recordProcessor.getBatchSize());
    }

    @Test
    public void testFlush() {
        // Setting restCallMaxNumberOfRecords to 5, so only restCallRecordsService gets called
        when(workerConfig.restCallMaxNumberOfRecords()).thenReturn(5);

        recordProcessor.add(record1);
        recordProcessor.add(record2);
        recordProcessor.add(record3);
        verifyZeroInteractions(batchFileRecordsService);
        verifyZeroInteractions(restCallRecordsService);
        assertEquals(3, recordProcessor.getBatchSize());

        assertEquals(Optional.empty(), recordProcessor.flush());
        assertEquals(0, recordProcessor.getBatchSize());

        verifyZeroInteractions(batchFileRecordsService);
        verify(restCallRecordsService).processRecords(anyCollection(), anyBoolean());

        recordProcessor.add(record1);
        recordProcessor.add(record2);
        recordProcessor.add(record3);
        verifyZeroInteractions(batchFileRecordsService);
        verifyZeroInteractions(restCallRecordsService);
        assertEquals(3, recordProcessor.getBatchSize());

        // Setting restCallMaxNumberOfRecords to 0, so only batchFileRecordsService gets called
        when(workerConfig.restCallMaxNumberOfRecords()).thenReturn(0);

        assertEquals(Optional.empty(), recordProcessor.flush());
        assertEquals(0, recordProcessor.getBatchSize());

        verifyZeroInteractions(restCallRecordsService);
        verify(batchFileRecordsService).processRecords(eq(Arrays.asList(record1.value(), record2.value(), record3.value())), eq(false));
    }

    private static class TestConsumerRecord<K, V> implements ConsumerRecord<K, V> {
        private final String topic;
        private final int partition;
        private final K key;
        private final V value;
        private final long offset;

        public TestConsumerRecord(String topic, int partition, K key, V value, long offset) {
            this.topic = topic;
            this.partition = partition;
            this.key = key;
            this.value = value;
            this.offset = offset;
        }

        @Override
        public String topic() {
            return topic;
        }

        @Override
        public int partition() {
            return partition;
        }

        @Override
        public K key() {
            return key;
        }

        @Override
        public V value() {
            return value;
        }

        @Override
        public long offset() {
            return offset;
        }

        @Override
        public long timestamp() {
            return 0;
        }
    }

}
