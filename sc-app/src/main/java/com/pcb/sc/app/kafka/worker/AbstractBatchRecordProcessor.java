package com.pcb.sc.app.kafka.worker;

import com.google.common.base.Preconditions;
import com.pcb.kafkacore.consumer.model.ConsumerRecord;
import com.pcb.sc.app.config.ScWorkerConfig;
import com.pcb.sc.app.kafka.utils.KafkaUtils;
import com.pcb.sc.service.BatchRecordProcessor;
import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public abstract class AbstractBatchRecordProcessor<MESSAGE, WORKER_CONFIG extends ScWorkerConfig> implements BatchRecordProcessor<String, MESSAGE> {

    private static final long STATUS_DUMP_INTERVAL_MILLIS = Duration.ofMinutes(1).toMillis();

    protected final Logger logger = LoggerFactory.getLogger(AbstractBatchRecordProcessor.class);
    protected final WORKER_CONFIG workerConfig;

    private final List<ConsumerRecord<String, MESSAGE>> pendingRequests = new ArrayList<>();
    private final AtomicBoolean shutdownRequested = new AtomicBoolean();
    private final AtomicInteger processedRecordCounter = new AtomicInteger();
    private final AtomicInteger testRecordCounter = new AtomicInteger();
    private final AtomicInteger flushCounter = new AtomicInteger();

    private long lastStatusDumpTs = DateTimeUtils.currentTimeMillis();

    protected AbstractBatchRecordProcessor(@Nonnull WORKER_CONFIG workerConfig) {
        this.workerConfig = Preconditions.checkNotNull(workerConfig, "config cannot be null");
    }

    @Override
    public void shutdown() {
        if (shutdownRequested.getAndSet(true))
            return;

        logger.info("Shutting down RecordProcessor");
    }

    @Nonnull
    @Override
    public Optional<AddFailureReason> add(ConsumerRecord<String, MESSAGE> record) {
        if (isValid(record)) {
            pendingRequests.add(record);
            return Optional.empty();
        } else {
            logger.trace("Got bad record: {}", record);
            return Optional.of(AddFailureReason.BAD_RECORD);
        }
    }

    @Nonnull
    @Override
    public Optional<FlushFailureReason> flush() {
        try {
            Map<Boolean, List<ConsumerRecord<String, MESSAGE>>> partitionedRecords = pendingRequests.stream().collect(Collectors.partitioningBy(record -> KafkaUtils.isMessageInTest(record.value())));
            List<ConsumerRecord<String, MESSAGE>> recordsToIgnore = partitionedRecords.getOrDefault(true, Collections.emptyList());
            List<ConsumerRecord<String, MESSAGE>> recordsToProcess = partitionedRecords.getOrDefault(false, Collections.emptyList());
            logger.trace("Got {} records to process and {} test records", recordsToProcess.size(), recordsToIgnore.size());
            Optional<FlushFailureReason> result = flush(recordsToProcess);
            incrementCounters(result.isEmpty() ? recordsToProcess.size() : 0, result.isEmpty() ? recordsToIgnore.size() : 0);
            logStatusIfNecessary();
            return result;
        } catch (RuntimeException e) {
            logger.error("Got an unexpected error while processing messages", e);
            return Optional.of(FlushFailureReason.UNEXPECTED_ERROR);
        }
    }

    @Override
    public void clear() {
        pendingRequests.clear();
    }

    @Override
    public int getBatchSize() {
        return pendingRequests.size();
    }

    protected Optional<FlushFailureReason> flush(List<ConsumerRecord<String, MESSAGE>> records) {
        process(records);
        return Optional.empty();
    }

    protected void process(List<ConsumerRecord<String, MESSAGE>> records) {
        if (!records.isEmpty()) {
            for (ConsumerRecord<String, MESSAGE> record : records) {
                process(record.value());
            }
        }

        clear();
    }

    private void incrementCounters(int numProcessedRecords, int numTestRecords) {
        processedRecordCounter.addAndGet(numProcessedRecords);
        testRecordCounter.addAndGet(numTestRecords);
        flushCounter.incrementAndGet();
    }

    private void logStatusIfNecessary() {
        long now = DateTimeUtils.currentTimeMillis();
        if (now - STATUS_DUMP_INTERVAL_MILLIS >= lastStatusDumpTs) {
            long elapsedTimeInSeconds = Duration.ofMillis(now - lastStatusDumpTs).toSeconds();
            lastStatusDumpTs = now;
            int totalProcessedRecords = processedRecordCounter.getAndSet(0);
            int totalTestRecords = testRecordCounter.getAndSet(0);
            int totalFlushes = flushCounter.getAndSet(0);
            logger.info("Got {} records to process and {} test records over the course of {} flush events in the last {} seconds", totalProcessedRecords, totalTestRecords, totalFlushes, elapsedTimeInSeconds);
        }
    }

    protected abstract boolean isValid(ConsumerRecord<String, MESSAGE> record);

    protected abstract void process(MESSAGE message);
}
