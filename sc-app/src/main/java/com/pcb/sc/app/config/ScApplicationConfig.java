package com.pcb.sc.app.config;

import com.google.common.base.Preconditions;
import com.pcb.sc.app.kafka.worker.WorkerType;
import com.pcb.sc.config.ApplicationConfig;
import com.pcb.sc.config.ConfigEntry;
import com.pcb.sc.config.WorkerConfig;
import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ScApplicationConfig implements ApplicationConfig {

    private static final Logger logger = LoggerFactory.getLogger(ScApplicationConfig.class);

    private static final ConfigEntry<String> APP_NAME_CONFIG_ENTRY = ConfigEntry.create("sc.applicationName", "sc");
    private static final ConfigEntry<Map<String, String>> WORKERS_CONFIG_ENTRY = ConfigEntry.create("sc.workers", Collections.emptyMap());

    private static final Pattern CONFIG_NAMESPACE_PATTERN = Pattern.compile("^(?<namespace>\\w+)(\\{(\\d+)\\.\\.(\\d+)})?$");
    private static final Pattern WORKER_NAME_PATTERN = Pattern.compile("^(?<workerIdBase>\\w+)(\\{(?<lowerBound>\\d+)\\.\\.(?<upperBound>\\d+)})?$");

    @Nonnull
    private final Configuration underlyingConfig;
    @Nonnull
    private final List<WorkerConfig> workerConfigs;

    public ScApplicationConfig(@Nonnull Configuration underlyingConfig) {
        this.underlyingConfig = underlyingConfig;
        this.workerConfigs = createWorkerConfigs(underlyingConfig);
    }

    @Nonnull
    @Override
    public String applicationName() {
        return underlyingConfig.getString(APP_NAME_CONFIG_ENTRY.getKey(), APP_NAME_CONFIG_ENTRY.getDefaultValue());
    }

    @Nonnull
    @Override
    public List<? extends WorkerConfig> workerConfigs() {
        return workerConfigs;
    }

    private static List<WorkerConfig> createWorkerConfigs(Configuration underlyingConfig) {
        Map<String, String> workerTypesById = new HashMap<>();
        String[] workerTypesStrArrayConfig = underlyingConfig.getStringArray(WORKERS_CONFIG_ENTRY.getKey());
        for (String workerTypeStrConfig : workerTypesStrArrayConfig) {
            workerTypesById.put(workerTypeStrConfig.split(":")[0], workerTypeStrConfig.split(":")[1]);
        }

        return workerTypesById.entrySet().stream()
                .flatMap(e -> {
                    String workerIdStr = e.getKey();
                    String workerTypeStr = e.getValue();
                    WorkerType workerType = WorkerType.fromString(workerTypeStr);
                    Preconditions.checkState(workerType != WorkerType.UNKNOWN, "Got unknown worker type: %s", workerTypeStr);
                    String namespace = getNamespace(workerIdStr);
                    List<String> workerIds = applyBraceExpansion(workerIdStr);
                    return workerIds.stream().map(workerId -> new ScWorkerConfig(underlyingConfig, workerId, workerType, namespace));
                })
                .collect(Collectors.toList());
    }

    private static String getNamespace(String workerIdStr) {
        Matcher matcher = CONFIG_NAMESPACE_PATTERN.matcher(workerIdStr);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Invalid worker ID '" + workerIdStr + "'; only alphanumeric names are permitted, with optional brace expansion (e.g., cis20Processor{1..4})");
        }

        String namespace = matcher.group("namespace");
        if (namespace == null) {
            throw new IllegalArgumentException("Invalid worker ID '" + workerIdStr + "'; only alphanumeric names are permitted, with optional brace expansion (e.g., cis20Processor{1..4})");
        }

        return namespace;
    }

    private static List<String> applyBraceExpansion(String workerIdStr) {
        Matcher matcher = WORKER_NAME_PATTERN.matcher(workerIdStr);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Invalid worker ID '" + workerIdStr + "'; only alphanumeric names are permitted, with optional brace expansion (e.g., cis20Processor{1..4})");
        }

        String workerIdBase = matcher.group("workerIdBase");
        String lowerBoundStr = matcher.group("lowerBound");
        String upperBoundStr = matcher.group("upperBound");
        if (lowerBoundStr == null || upperBoundStr == null) {
            return Collections.singletonList(workerIdBase);
        }

        int lowerBound = Integer.parseInt(lowerBoundStr);
        int upperBound = Integer.parseInt(upperBoundStr);

        if (lowerBound > upperBound) {
            throw new IllegalArgumentException("Invalid worker ID '" + workerIdStr + "'; first bound must be less than or equal to the second bound");
        }

        List<String> workerIds = IntStream.range(lowerBound, upperBound + 1)
                .mapToObj(i -> workerIdBase + i)
                .collect(Collectors.toList());

        logger.info("Expanded '" + workerIdStr + "' to these worker IDs: " + workerIds);
        return workerIds;
    }
}
