package com.pcb.sc.app.service.rest;

import com.pcb.sc.app.model.Cis20Record;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ScoringServerService {

    @POST("CIS20/")
    Call<Void> publishRecord(@Body Cis20Record record);

}
