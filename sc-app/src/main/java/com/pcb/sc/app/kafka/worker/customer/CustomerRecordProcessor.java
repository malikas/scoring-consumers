package com.pcb.sc.app.kafka.worker.customer;

import com.pcb.kafkacore.consumer.model.ConsumerRecord;
import com.pcb.sc.app.config.ScWorkerConfig;
import com.pcb.sc.app.kafka.worker.AbstractBatchRecordProcessor;
import com.pcb.sc.app.model.Customer;

import javax.annotation.Nonnull;

public class CustomerRecordProcessor extends AbstractBatchRecordProcessor<Customer, ScWorkerConfig> {

    CustomerRecordProcessor(@Nonnull ScWorkerConfig workerConfig) {
        super(workerConfig);
    }

    @Override
    protected boolean isValid(ConsumerRecord<String, Customer> record) {
        return record != null
                && record.value() != null;
    }

    @Override
    protected void process(Customer customer) {
        logger.info("CustomerRecordProcessor is processing record with the value: [{}]", customer);
    }
}
