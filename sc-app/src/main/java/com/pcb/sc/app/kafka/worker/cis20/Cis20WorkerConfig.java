package com.pcb.sc.app.kafka.worker.cis20;

import com.pcb.sc.app.config.ScWorkerConfig;
import com.pcb.sc.app.kafka.worker.WorkerType;
import com.pcb.sc.config.ConfigEntry;
import com.pcb.sc.config.WorkerConfig;
import org.apache.commons.configuration2.Configuration;
import org.jetbrains.annotations.NotNull;

public class Cis20WorkerConfig extends ScWorkerConfig {

    private static final String BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX = "BatchFileRecordsService.";
    private static final String REST_CALL_RECORDS_SERVICE_CONFIG_KEY_PREFIX = "RestCallRecordsService.";

    private static final ConfigEntry<String> BATCH_FILE_PATH_CONFIG_ENTRY = ConfigEntry.create(BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "filePath", "./");
    private static final ConfigEntry<String> BATCH_FILE_PREFIX_CONFIG_ENTRY = ConfigEntry.create(BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "filePrefix", "");
    private static final ConfigEntry<String> BATCH_FILE_POSTFIX_CONFIG_ENTRY = ConfigEntry.create(BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "filePostfix", "");
    private static final ConfigEntry<String> BATCH_FILE_HEADER_CONFIG_ENTRY = ConfigEntry.create(BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "fileHeader", "");
    private static final ConfigEntry<String> BATCH_FILE_FOOTER_CONFIG_ENTRY = ConfigEntry.create(BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "fileFooter", "");
    private static final ConfigEntry<Integer> BATCH_FILE_INTERNAL_QUEUE_CAPACITY_CONFIG_ENTRY = ConfigEntry.create(BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "internalQueueCapacity", 15_000);
    private static final ConfigEntry<Integer> BATCH_FILE_DUMP_THRESHOLD_CONFIG_ENTRY = ConfigEntry.create(BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "dumpThreshold", 10_000);
    private static final ConfigEntry<Integer> BATCH_FILE_DUMP_INTERVAL_IN_SECONDS_CONFIG_ENTRY = ConfigEntry.create(BATCH_FILE_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "dumpIntervalInSeconds", 120);

    private static final ConfigEntry<Integer> REST_CALL_MAX_NUMBER_OF_RECORDS_CONFIG_ENTRY = ConfigEntry.create(REST_CALL_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "maxNumberOfRecords", 10);
    private static final ConfigEntry<String> REST_CALL_SCORING_SERVER_URL_CONFIG_ENTRY = ConfigEntry.create(REST_CALL_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "scoringServerUrl", "");
    private static final ConfigEntry<String> REST_CALL_FILE_ERROR_PATH_CONFIG_ENTRY = ConfigEntry.create(REST_CALL_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "fileErrorPath", "./");
    private static final ConfigEntry<String> REST_CALL_FILE_ERROR_NAME_CONFIG_ENTRY = ConfigEntry.create(REST_CALL_RECORDS_SERVICE_CONFIG_KEY_PREFIX + "fileErrorName", "scoringServerFailedRequests.txt");

    public Cis20WorkerConfig(@NotNull WorkerConfig workerConfig) {
        super(workerConfig.underlyingConfig(), workerConfig.workerId(), workerConfig.workerType(), workerConfig.namespace());
    }

    public Cis20WorkerConfig(@NotNull Configuration underlyingConfig, @NotNull String workerId, @NotNull WorkerType workerType, @NotNull String namespace) {
        super(underlyingConfig, workerId, workerType, namespace);

        underlyingConfig.setProperty(BATCH_FILE_PATH_CONFIG_ENTRY.getKey(), batchFilePath());
        underlyingConfig.setProperty(BATCH_FILE_PREFIX_CONFIG_ENTRY.getKey(), batchFilePrefix());
        underlyingConfig.setProperty(BATCH_FILE_POSTFIX_CONFIG_ENTRY.getKey(), batchFilePostfix());
        underlyingConfig.setProperty(BATCH_FILE_HEADER_CONFIG_ENTRY.getKey(), batchFileHeader());
        underlyingConfig.setProperty(BATCH_FILE_FOOTER_CONFIG_ENTRY.getKey(), batchFileFooter());
        underlyingConfig.setProperty(BATCH_FILE_INTERNAL_QUEUE_CAPACITY_CONFIG_ENTRY.getKey(), batchFileInternalQueueCapacity());
        underlyingConfig.setProperty(BATCH_FILE_DUMP_THRESHOLD_CONFIG_ENTRY.getKey(), batchFileDumpThreshold());
        underlyingConfig.setProperty(BATCH_FILE_DUMP_INTERVAL_IN_SECONDS_CONFIG_ENTRY.getKey(), batchFileDumpIntervalInSeconds());

        underlyingConfig.setProperty(REST_CALL_MAX_NUMBER_OF_RECORDS_CONFIG_ENTRY.getKey(), restCallMaxNumberOfRecords());
        underlyingConfig.setProperty(REST_CALL_SCORING_SERVER_URL_CONFIG_ENTRY.getKey(), restCallScoringServerUrl());
        underlyingConfig.setProperty(REST_CALL_FILE_ERROR_PATH_CONFIG_ENTRY.getKey(), restCallFileErrorPath());
        underlyingConfig.setProperty(REST_CALL_FILE_ERROR_NAME_CONFIG_ENTRY.getKey(), restCallFileErrorName());
    }

    public String batchFilePath() {
        return getString(BATCH_FILE_PATH_CONFIG_ENTRY);
    }

    public String batchFilePrefix() {
        return getString(BATCH_FILE_PREFIX_CONFIG_ENTRY);
    }

    public String batchFilePostfix() {
        return getString(BATCH_FILE_POSTFIX_CONFIG_ENTRY);
    }

    public String batchFileHeader() {
        return getString(BATCH_FILE_HEADER_CONFIG_ENTRY);
    }

    public String batchFileFooter() {
        return getString(BATCH_FILE_FOOTER_CONFIG_ENTRY);
    }

    public Integer batchFileInternalQueueCapacity() {
        return getInteger(BATCH_FILE_INTERNAL_QUEUE_CAPACITY_CONFIG_ENTRY);
    }

    public Integer batchFileDumpThreshold() {
        return getInteger(BATCH_FILE_DUMP_THRESHOLD_CONFIG_ENTRY);
    }

    public Integer batchFileDumpIntervalInSeconds() {
        return getInteger(BATCH_FILE_DUMP_INTERVAL_IN_SECONDS_CONFIG_ENTRY);
    }

    public Integer restCallMaxNumberOfRecords() {
        return getInteger(REST_CALL_MAX_NUMBER_OF_RECORDS_CONFIG_ENTRY);
    }

    public String restCallScoringServerUrl() {
        return getString(REST_CALL_SCORING_SERVER_URL_CONFIG_ENTRY);
    }

    public String restCallFileErrorPath() {
        return getString(REST_CALL_FILE_ERROR_PATH_CONFIG_ENTRY);
    }

    public String restCallFileErrorName() {
        return getString(REST_CALL_FILE_ERROR_NAME_CONFIG_ENTRY);
    }

}
