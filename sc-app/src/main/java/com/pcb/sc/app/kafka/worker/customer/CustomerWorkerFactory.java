package com.pcb.sc.app.kafka.worker.customer;

import com.pcb.sc.app.config.ScWorkerConfig;
import com.pcb.sc.app.model.Customer;
import com.pcb.sc.config.WorkerConfig;
import com.pcb.sc.kafka.worker.AbstractWorkerFactory;
import com.pcb.sc.service.BatchRecordProcessor;

import javax.annotation.Nonnull;

public class CustomerWorkerFactory extends AbstractWorkerFactory<String, Customer> {

    public CustomerWorkerFactory() {
        super(null, null);
    }

    @Override
    protected BatchRecordProcessor<String, Customer> recordProcessor(@Nonnull WorkerConfig workerConfig) {
        return new CustomerRecordProcessor(new ScWorkerConfig(workerConfig));
    }
}
