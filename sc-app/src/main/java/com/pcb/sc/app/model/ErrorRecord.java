package com.pcb.sc.app.model;

public class ErrorRecord {

    private Long requestTimestamp;
    private String requestBody;
    private Integer responseCode;

    private ErrorRecord(Builder builder) {
        this.requestTimestamp = builder.requestTimestamp;
        this.requestBody = builder.requestBody;
        this.responseCode = builder.responseCode;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ErrorRecord{");
        sb.append("requestTimestamp=").append(requestTimestamp);
        sb.append(", requestBody='").append(requestBody).append('\'');
        sb.append(", responseCode=").append(responseCode);
        sb.append('}');
        return sb.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Long requestTimestamp;
        private String requestBody;
        private Integer responseCode;

        private Builder() {
        }

        public Builder requestTimestamp(Long requestTimestamp) {
            this.requestTimestamp = requestTimestamp;
            return this;
        }

        public Builder requestBody(String requestBody) {
            this.requestBody = requestBody;
            return this;
        }

        public Builder responseCode(Integer responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        public ErrorRecord build() {
            return new ErrorRecord(this);
        }
    }
}
