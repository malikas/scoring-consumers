package com.pcb.sc.app.kafka.worker.test;

import com.pcb.kafkacore.consumer.model.ConsumerRecord;
import com.pcb.sc.app.config.ScWorkerConfig;
import com.pcb.sc.app.kafka.worker.AbstractBatchRecordProcessor;

import javax.annotation.Nonnull;

public class TestRecordProcessor extends AbstractBatchRecordProcessor<String, ScWorkerConfig> {

    TestRecordProcessor(@Nonnull ScWorkerConfig workerConfig) {
        super(workerConfig);
    }

    @Override
    protected boolean isValid(ConsumerRecord<String, String> record) {
        return record != null
//                && record.key() != null
                && record.value() != null;
    }

    @Override
    protected void process(String s) {
        logger.info("TestRecordProcessor is processing record with the value [{}]", s);
    }
}
