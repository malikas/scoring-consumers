package com.pcb.sc.app.config;

import com.pcb.sc.app.kafka.worker.WorkerType;
import com.pcb.sc.config.WorkerConfig;
import org.apache.commons.configuration2.Configuration;

import javax.annotation.Nonnull;

public class ScWorkerConfig extends WorkerConfig {

    private static final String WORKER_CONFIG_KEY_PREFIX = "sc.worker.";

    public ScWorkerConfig(@Nonnull WorkerConfig workerConfig) {
        super(workerConfig.underlyingConfig(), workerConfig.workerId(), workerConfig.workerType(), workerConfig.namespace());
    }

    public ScWorkerConfig(@Nonnull Configuration underlyingConfig, @Nonnull String workerId, @Nonnull WorkerType workerType, @Nonnull String namespace) {
        super(underlyingConfig, workerId, workerType, namespace);
    }

    @Nonnull
    @Override
    protected String getConfigKeyPrefix() {
        return WORKER_CONFIG_KEY_PREFIX;
    }
}
