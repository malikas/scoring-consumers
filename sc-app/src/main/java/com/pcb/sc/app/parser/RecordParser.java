package com.pcb.sc.app.parser;

public interface RecordParser<ACCEPT, RETURN> {

    RETURN parseRecord(ACCEPT record);

}
