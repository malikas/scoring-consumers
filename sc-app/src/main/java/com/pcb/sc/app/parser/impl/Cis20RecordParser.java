package com.pcb.sc.app.parser.impl;

import com.pcb.sc.app.model.Cis20Record;
import com.pcb.sc.app.parser.RecordParser;
import io.vavr.Tuple2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BiConsumer;

public class Cis20RecordParser implements RecordParser<String, Cis20Record> {

    private static final Logger logger = LoggerFactory.getLogger(Cis20RecordParser.class);

    private final Map<String, Tuple2<List<Integer>, BiConsumer<Cis20Record.Builder, String>>> fieldMappings;

    public Cis20RecordParser() {
        this.fieldMappings = new LinkedHashMap<>();
        this.fieldMappings.put("workflow", new Tuple2<>(Arrays.asList(0, 15), Cis20Record.Builder::workflow));
        this.fieldMappings.put("recordType", new Tuple2<>(Arrays.asList(16, 23), Cis20Record.Builder::recordType));
        this.fieldMappings.put("dataSpecificationVersion", new Tuple2<>(Arrays.asList(24, 28), Cis20Record.Builder::dataSpecificationVersion));
        this.fieldMappings.put("clientIdFromHeader", new Tuple2<>(Arrays.asList(29, 44), Cis20Record.Builder::clientIdFromHeader));
        this.fieldMappings.put("recordCreationDate", new Tuple2<>(Arrays.asList(45, 52), Cis20Record.Builder::recordCreationDate));
        this.fieldMappings.put("recordCreationTime", new Tuple2<>(Arrays.asList(53, 58), Cis20Record.Builder::recordCreationTime));
        this.fieldMappings.put("recordCreationMilliseconds", new Tuple2<>(Arrays.asList(59, 61), Cis20Record.Builder::recordCreationMilliseconds));
        this.fieldMappings.put("gmtOffset", new Tuple2<>(Arrays.asList(62, 67), Cis20Record.Builder::gmtOffset));
        this.fieldMappings.put("customerIdFromHeader", new Tuple2<>(Arrays.asList(68, 87), Cis20Record.Builder::customerIdFromHeader));
        this.fieldMappings.put("customerAcctNumber", new Tuple2<>(Arrays.asList(88, 127), Cis20Record.Builder::customerAcctNumber));
        this.fieldMappings.put("externalTransactionId", new Tuple2<>(Arrays.asList(128, 159), Cis20Record.Builder::externalTransactionId));
        this.fieldMappings.put("customerType", new Tuple2<>(Arrays.asList(160, 160), Cis20Record.Builder::customerType));
        this.fieldMappings.put("vipType", new Tuple2<>(Arrays.asList(161, 161), Cis20Record.Builder::vipType));
        this.fieldMappings.put("relationshipStartDate", new Tuple2<>(Arrays.asList(162, 169), Cis20Record.Builder::relationshipStartDate));
        this.fieldMappings.put("numberOfAccounts", new Tuple2<>(Arrays.asList(170, 174), Cis20Record.Builder::numberOfAccounts));
        this.fieldMappings.put("givenName", new Tuple2<>(Arrays.asList(175, 204), Cis20Record.Builder::givenName));
        this.fieldMappings.put("middleName", new Tuple2<>(Arrays.asList(205, 234), Cis20Record.Builder::middleName));
        this.fieldMappings.put("surname", new Tuple2<>(Arrays.asList(235, 294), Cis20Record.Builder::surname));
        this.fieldMappings.put("title", new Tuple2<>(Arrays.asList(295, 304), Cis20Record.Builder::title));
        this.fieldMappings.put("suffix", new Tuple2<>(Arrays.asList(305, 314), Cis20Record.Builder::suffix));
        this.fieldMappings.put("preferredGreeting", new Tuple2<>(Arrays.asList(315, 374), Cis20Record.Builder::preferredGreeting));
        this.fieldMappings.put("preferredLanguage", new Tuple2<>(Arrays.asList(375, 377), Cis20Record.Builder::preferredLanguage));
        this.fieldMappings.put("mothersMaidenName", new Tuple2<>(Arrays.asList(378, 437), Cis20Record.Builder::mothersMaidenName));
        this.fieldMappings.put("householdName", new Tuple2<>(Arrays.asList(438, 497), Cis20Record.Builder::householdName));
        this.fieldMappings.put("streetLine1", new Tuple2<>(Arrays.asList(498, 537), Cis20Record.Builder::streetLine1));
        this.fieldMappings.put("streetLine2", new Tuple2<>(Arrays.asList(538, 577), Cis20Record.Builder::streetLine2));
        this.fieldMappings.put("streetLine3", new Tuple2<>(Arrays.asList(578, 617), Cis20Record.Builder::streetLine3));
        this.fieldMappings.put("streetLine4", new Tuple2<>(Arrays.asList(618, 657), Cis20Record.Builder::streetLine4));
        this.fieldMappings.put("city", new Tuple2<>(Arrays.asList(658, 697), Cis20Record.Builder::city));
        this.fieldMappings.put("stateProvince", new Tuple2<>(Arrays.asList(698, 700), Cis20Record.Builder::stateProvince));
        this.fieldMappings.put("postalCode", new Tuple2<>(Arrays.asList(701, 710), Cis20Record.Builder::postalCode));
        this.fieldMappings.put("countryCode", new Tuple2<>(Arrays.asList(711, 713), Cis20Record.Builder::countryCode));
        this.fieldMappings.put("residenceStatus", new Tuple2<>(Arrays.asList(714, 714), Cis20Record.Builder::residenceStatus));
        this.fieldMappings.put("dateAtAddress", new Tuple2<>(Arrays.asList(715, 722), Cis20Record.Builder::dateAtAddress));
        this.fieldMappings.put("secondaryAddrType", new Tuple2<>(Arrays.asList(723, 723), Cis20Record.Builder::secondaryAddrType));
        this.fieldMappings.put("secondaryAddrStreetLine1", new Tuple2<>(Arrays.asList(724, 763), Cis20Record.Builder::secondaryAddrStreetLine1));
        this.fieldMappings.put("secondaryAddrStreetLine2", new Tuple2<>(Arrays.asList(764, 803), Cis20Record.Builder::secondaryAddrStreetLine2));
        this.fieldMappings.put("secondaryAddrStreetLine3", new Tuple2<>(Arrays.asList(804, 843), Cis20Record.Builder::secondaryAddrStreetLine3));
        this.fieldMappings.put("secondaryAddrStreetLine4", new Tuple2<>(Arrays.asList(844, 883), Cis20Record.Builder::secondaryAddrStreetLine4));
        this.fieldMappings.put("secondaryAddrCity", new Tuple2<>(Arrays.asList(884, 923), Cis20Record.Builder::secondaryAddrCity));
        this.fieldMappings.put("secondaryAddrStateProvince", new Tuple2<>(Arrays.asList(924, 926), Cis20Record.Builder::secondaryAddrStateProvince));
        this.fieldMappings.put("secondaryAddrPostalCode", new Tuple2<>(Arrays.asList(927, 936), Cis20Record.Builder::secondaryAddrPostalCode));
        this.fieldMappings.put("secondaryAddrCountryCode", new Tuple2<>(Arrays.asList(937, 939), Cis20Record.Builder::secondaryAddrCountryCode));
        this.fieldMappings.put("employer", new Tuple2<>(Arrays.asList(940, 999), Cis20Record.Builder::employer));
        this.fieldMappings.put("workAddrStreetLine1", new Tuple2<>(Arrays.asList(1000, 1039), Cis20Record.Builder::workAddrStreetLine1));
        this.fieldMappings.put("workAddrStreetLine2", new Tuple2<>(Arrays.asList(1040, 1079), Cis20Record.Builder::workAddrStreetLine2));
        this.fieldMappings.put("workAddrStreetLine3", new Tuple2<>(Arrays.asList(1080, 1119), Cis20Record.Builder::workAddrStreetLine3));
        this.fieldMappings.put("workAddrStreetLine4", new Tuple2<>(Arrays.asList(1120, 1159), Cis20Record.Builder::workAddrStreetLine4));
        this.fieldMappings.put("workAddrCity", new Tuple2<>(Arrays.asList(1160, 1199), Cis20Record.Builder::workAddrCity));
        this.fieldMappings.put("workAddrStateProvince", new Tuple2<>(Arrays.asList(1200, 1202), Cis20Record.Builder::workAddrStateProvince));
        this.fieldMappings.put("workAddrPostalCode", new Tuple2<>(Arrays.asList(1203, 1212), Cis20Record.Builder::workAddrPostalCode));
        this.fieldMappings.put("workAddrCountryCode", new Tuple2<>(Arrays.asList(1213, 1215), Cis20Record.Builder::workAddrCountryCode));
        this.fieldMappings.put("employmentStatus", new Tuple2<>(Arrays.asList(1216, 1218), Cis20Record.Builder::employmentStatus));
        this.fieldMappings.put("employmentStartDate", new Tuple2<>(Arrays.asList(1219, 1226), Cis20Record.Builder::employmentStartDate));
        this.fieldMappings.put("employerMcc", new Tuple2<>(Arrays.asList(1227, 1230), Cis20Record.Builder::employerMcc));
        this.fieldMappings.put("occupationCode", new Tuple2<>(Arrays.asList(1231, 1234), Cis20Record.Builder::occupationCode));
        this.fieldMappings.put("income", new Tuple2<>(Arrays.asList(1235, 1250), Cis20Record.Builder::income));
        this.fieldMappings.put("currencyCode", new Tuple2<>(Arrays.asList(1251, 1253), Cis20Record.Builder::currencyCode));
        this.fieldMappings.put("currencyConversionRate", new Tuple2<>(Arrays.asList(1254, 1266), Cis20Record.Builder::currencyConversionRate));
        this.fieldMappings.put("homePhone", new Tuple2<>(Arrays.asList(1267, 1290), Cis20Record.Builder::homePhone));
        this.fieldMappings.put("secondaryPhone", new Tuple2<>(Arrays.asList(1291, 1314), Cis20Record.Builder::secondaryPhone));
        this.fieldMappings.put("workPhone", new Tuple2<>(Arrays.asList(1315, 1338), Cis20Record.Builder::workPhone));
        this.fieldMappings.put("mobilePhone", new Tuple2<>(Arrays.asList(1339, 1362), Cis20Record.Builder::mobilePhone));
        this.fieldMappings.put("preferredPhone", new Tuple2<>(Arrays.asList(1363, 1363), Cis20Record.Builder::preferredPhone));
        this.fieldMappings.put("emailAddress", new Tuple2<>(Arrays.asList(1364, 1403), Cis20Record.Builder::emailAddress));
        this.fieldMappings.put("educationalStatus", new Tuple2<>(Arrays.asList(1404, 1404), Cis20Record.Builder::educationalStatus));
        this.fieldMappings.put("birthDate", new Tuple2<>(Arrays.asList(1405, 1412), Cis20Record.Builder::birthDate));
        this.fieldMappings.put("birthCountry", new Tuple2<>(Arrays.asList(1413, 1415), Cis20Record.Builder::birthCountry));
        this.fieldMappings.put("citizenshipCountry", new Tuple2<>(Arrays.asList(1416, 1418), Cis20Record.Builder::citizenshipCountry));
        this.fieldMappings.put("nationalId", new Tuple2<>(Arrays.asList(1419, 1434), Cis20Record.Builder::nationalId));
        this.fieldMappings.put("nationalIdCountry", new Tuple2<>(Arrays.asList(1435, 1437), Cis20Record.Builder::nationalIdCountry));
        this.fieldMappings.put("passportNumber", new Tuple2<>(Arrays.asList(1438, 1453), Cis20Record.Builder::passportNumber));
        this.fieldMappings.put("passportCountry", new Tuple2<>(Arrays.asList(1454, 1456), Cis20Record.Builder::passportCountry));
        this.fieldMappings.put("passportExpirationDate", new Tuple2<>(Arrays.asList(1457, 1464), Cis20Record.Builder::passportExpirationDate));
        this.fieldMappings.put("driversLicenseNumber", new Tuple2<>(Arrays.asList(1465, 1480), Cis20Record.Builder::driversLicenseNumber));
        this.fieldMappings.put("driversLicenseCountry", new Tuple2<>(Arrays.asList(1481, 1483), Cis20Record.Builder::driversLicenseCountry));
        this.fieldMappings.put("taxId", new Tuple2<>(Arrays.asList(1484, 1499), Cis20Record.Builder::taxId));
        this.fieldMappings.put("taxIdCountry", new Tuple2<>(Arrays.asList(1500, 1502), Cis20Record.Builder::taxIdCountry));
        this.fieldMappings.put("gender", new Tuple2<>(Arrays.asList(1503, 1503), Cis20Record.Builder::gender));
        this.fieldMappings.put("maritalStatus", new Tuple2<>(Arrays.asList(1504, 1504), Cis20Record.Builder::maritalStatus));
        this.fieldMappings.put("numberOfDependents", new Tuple2<>(Arrays.asList(1505, 1506), Cis20Record.Builder::numberOfDependents));
        this.fieldMappings.put("creditScore", new Tuple2<>(Arrays.asList(1507, 1510), Cis20Record.Builder::creditScore));
        this.fieldMappings.put("creditScoreDate", new Tuple2<>(Arrays.asList(1511, 1518), Cis20Record.Builder::creditScoreDate));
        this.fieldMappings.put("creditScoreSource", new Tuple2<>(Arrays.asList(1519, 1538), Cis20Record.Builder::creditScoreSource));
        this.fieldMappings.put("creditScoreRequestReason", new Tuple2<>(Arrays.asList(1539, 1539), Cis20Record.Builder::creditScoreRequestReason));
        this.fieldMappings.put("creditRating", new Tuple2<>(Arrays.asList(1540, 1543), Cis20Record.Builder::creditRating));
        this.fieldMappings.put("pefp", new Tuple2<>(Arrays.asList(1544, 1544), Cis20Record.Builder::pefp));
        this.fieldMappings.put("ofac", new Tuple2<>(Arrays.asList(1545, 1545), Cis20Record.Builder::ofac));
        this.fieldMappings.put("behaviorScore1", new Tuple2<>(Arrays.asList(1546, 1549), Cis20Record.Builder::behaviorScore1));
        this.fieldMappings.put("behaviorScore2", new Tuple2<>(Arrays.asList(1550, 1553), Cis20Record.Builder::behaviorScore2));
        this.fieldMappings.put("segmentId1", new Tuple2<>(Arrays.asList(1554, 1559), Cis20Record.Builder::segmentId1));
        this.fieldMappings.put("segmentId2", new Tuple2<>(Arrays.asList(1560, 1565), Cis20Record.Builder::segmentId2));
        this.fieldMappings.put("segmentId3", new Tuple2<>(Arrays.asList(1566, 1571), Cis20Record.Builder::segmentId3));
        this.fieldMappings.put("segmentId4", new Tuple2<>(Arrays.asList(1572, 1577), Cis20Record.Builder::segmentId4));
        this.fieldMappings.put("userIndicator01", new Tuple2<>(Arrays.asList(1578, 1578), Cis20Record.Builder::userIndicator01));
        this.fieldMappings.put("userIndicator02", new Tuple2<>(Arrays.asList(1579, 1579), Cis20Record.Builder::userIndicator02));
        this.fieldMappings.put("userIndicator03", new Tuple2<>(Arrays.asList(1580, 1580), Cis20Record.Builder::userIndicator03));
        this.fieldMappings.put("userIndicator04", new Tuple2<>(Arrays.asList(1581, 1581), Cis20Record.Builder::userIndicator04));
        this.fieldMappings.put("userIndicator05", new Tuple2<>(Arrays.asList(1582, 1582), Cis20Record.Builder::userIndicator05));
        this.fieldMappings.put("userCode1", new Tuple2<>(Arrays.asList(1583, 1585), Cis20Record.Builder::userCode1));
        this.fieldMappings.put("userCode2", new Tuple2<>(Arrays.asList(1586, 1588), Cis20Record.Builder::userCode2));
        this.fieldMappings.put("userCode3", new Tuple2<>(Arrays.asList(1589, 1591), Cis20Record.Builder::userCode3));
        this.fieldMappings.put("userCode4", new Tuple2<>(Arrays.asList(1592, 1594), Cis20Record.Builder::userCode4));
        this.fieldMappings.put("userCode5", new Tuple2<>(Arrays.asList(1595, 1597), Cis20Record.Builder::userCode5));
        this.fieldMappings.put("userData01", new Tuple2<>(Arrays.asList(1598, 1603), Cis20Record.Builder::userData01));
        this.fieldMappings.put("userData02", new Tuple2<>(Arrays.asList(1604, 1609), Cis20Record.Builder::userData02));
        this.fieldMappings.put("userData03", new Tuple2<>(Arrays.asList(1610, 1615), Cis20Record.Builder::userData03));
        this.fieldMappings.put("userData04", new Tuple2<>(Arrays.asList(1616, 1623), Cis20Record.Builder::userData04));
        this.fieldMappings.put("userData05", new Tuple2<>(Arrays.asList(1624, 1631), Cis20Record.Builder::userData05));
        this.fieldMappings.put("userData06", new Tuple2<>(Arrays.asList(1632, 1639), Cis20Record.Builder::userData06));
        this.fieldMappings.put("userData07", new Tuple2<>(Arrays.asList(1640, 1649), Cis20Record.Builder::userData07));
        this.fieldMappings.put("userData08", new Tuple2<>(Arrays.asList(1650, 1659), Cis20Record.Builder::userData08));
        this.fieldMappings.put("userData09", new Tuple2<>(Arrays.asList(1660, 1674), Cis20Record.Builder::userData09));
        this.fieldMappings.put("userData10", new Tuple2<>(Arrays.asList(1675, 1689), Cis20Record.Builder::userData10));
        this.fieldMappings.put("userData11", new Tuple2<>(Arrays.asList(1690, 1709), Cis20Record.Builder::userData11));
        this.fieldMappings.put("userData12", new Tuple2<>(Arrays.asList(1710, 1729), Cis20Record.Builder::userData12));
        this.fieldMappings.put("userData13", new Tuple2<>(Arrays.asList(1730, 1769), Cis20Record.Builder::userData13));
        this.fieldMappings.put("userData14", new Tuple2<>(Arrays.asList(1770, 1809), Cis20Record.Builder::userData14));
        this.fieldMappings.put("userData15", new Tuple2<>(Arrays.asList(1810, 1869), Cis20Record.Builder::userData15));
        this.fieldMappings.put("RESERVED_01", new Tuple2<>(Arrays.asList(1870, 1899), Cis20Record.Builder::RESERVED_01));
    }

    @Override
    public Cis20Record parseRecord(String record) {
        Objects.requireNonNull(record);

        Cis20Record.Builder cis20RecordBuilder = Cis20Record.builder();

        for (Map.Entry<String, Tuple2<List<Integer>, BiConsumer<Cis20Record.Builder, String>>> entry : fieldMappings.entrySet()) {
            String fieldValue = parseRecord(record, entry.getValue()._1.get(0), entry.getValue()._1.get(1));
            if (fieldValue != null) {
                fieldValue = fieldValue.trim();
                entry.getValue()._2.accept(cis20RecordBuilder, fieldValue);
            }
        }

        return cis20RecordBuilder.build();
    }

    private String parseRecord(String record, int startIndex, int endIndex) {
        Objects.requireNonNull(record);

        if (startIndex < 0 || endIndex < 0 || startIndex > record.length() || endIndex > record.length() || startIndex > endIndex) {
            return null;
        }

        return record.substring(startIndex, endIndex + 1);
    }
}
