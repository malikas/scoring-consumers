package com.pcb.sc.app.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.MoreExecutors;
import com.pcb.sc.app.service.RecordsService;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.io.FileUtils;
import org.glassfish.jersey.internal.guava.ThreadFactoryBuilder;
import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class BatchFileRecordsService<MESSAGE> implements RecordsService<MESSAGE> {

    private static final Logger logger = LoggerFactory.getLogger(BatchFileRecordsService.class);

    private static final String CONFIG_KEY_PREFIX = "BatchFileRecordsService.";

    private static final String FILE_PATH_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "filePath";
    private static final String FILE_PREFIX_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "filePrefix";
    private static final String FILE_POSTFIX_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "filePostfix";
    private static final String FILE_HEADER_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "fileHeader";
    private static final String FILE_FOOTER_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "fileFooter";
    private static final String INTERNAL_QUEUE_CAPACITY_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "internalQueueCapacity";
    private static final String DUMP_THRESHOLD_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "dumpThreshold";
    private static final String DUMP_INTERVAL_IN_SECONDS_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "dumpIntervalInSeconds";

    private final Configuration config;

    private final ExecutorService workerExecutor;
    private final ExecutorService threadPoolExecutor;
    private final BlockingQueue<MESSAGE> pendingRecordsQueue;

    private final AtomicLong lastDumpTime = new AtomicLong();
    private final AtomicBoolean shutdown = new AtomicBoolean();

    public BatchFileRecordsService(@Nonnull Configuration config) {
        this.config = Preconditions.checkNotNull(config, "config cannot be null");

        this.workerExecutor = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().setNameFormat("BatchFileRecordsServiceWorkerExecutor-%d").build());
        this.threadPoolExecutor = Executors.newFixedThreadPool(20, new ThreadFactoryBuilder().setNameFormat("BatchFileRecordsServiceThreadPoolExecutor-%d").build());
        this.pendingRecordsQueue = new ArrayBlockingQueue<>(config.getInt(INTERNAL_QUEUE_CAPACITY_CONFIG_ENTRY));

        this.lastDumpTime.set(DateTimeUtils.currentTimeMillis());
        this.shutdown.set(false);

        workerExecutor.submit(() -> {
            while (!shutdown.get() && !Thread.currentThread().isInterrupted()) {
                start();
            }
        });
    }

    @Override
    public void start() {
        long intervalBetweenDumpsInMillis = config.getInt(DUMP_INTERVAL_IN_SECONDS_CONFIG_ENTRY) * 1000L;
        long lastDumpInMillis = lastDumpTime.get();

        if (pendingRecordsQueue.size() > config.getInt(DUMP_THRESHOLD_CONFIG_ENTRY)) {
            logger.info("The internal queue size has reached the threshold of {}. Proceeding to dump the records", config.getInt(DUMP_THRESHOLD_CONFIG_ENTRY));
            drainRecords();
        } else if (DateTimeUtils.currentTimeMillis() - lastDumpInMillis >= intervalBetweenDumpsInMillis) {
            logger.info("The interval of {} seconds has passed since the last dump at {}. Proceeding to dump the records", intervalBetweenDumpsInMillis / 1000, lastDumpInMillis);
            drainRecords();
        }
    }

    @Override
    public void shutdown() {
        if (shutdown.getAndSet(true)) {
            return;
        }

        // Trigger the last drain before shutdown
        drainRecords();
        MoreExecutors.shutdownAndAwaitTermination(workerExecutor, 1, TimeUnit.MINUTES);
        MoreExecutors.shutdownAndAwaitTermination(threadPoolExecutor, 1, TimeUnit.MINUTES);
    }

    private void drainRecords() {
        lastDumpTime.set(DateTimeUtils.currentTimeMillis());

        if (pendingRecordsQueue.isEmpty()) {
            logger.info("The internal queue doesn't contain any messages. Skipping the dump");
            return;
        }

        List<MESSAGE> recordsToDump = new ArrayList<>();
        pendingRecordsQueue.drainTo(recordsToDump);
        dumpRecordsIntoBatchFile(recordsToDump);
    }

    private void dumpRecordsIntoBatchFile(List<MESSAGE> records) {
        boolean success = false;
        int numberOfAttempts = 0;

        while (!success) {
            numberOfAttempts++;

            String fileName = config.getString(FILE_PREFIX_CONFIG_ENTRY) + DateTimeUtils.currentTimeMillis() + ".dat";
            String header = config.getString(FILE_HEADER_CONFIG_ENTRY);
            String footer = config.getString(FILE_FOOTER_CONFIG_ENTRY) + String.format("  %08d", records.size());
            logger.info("Dumping {} records into the new file: {}", records.size(), fileName);
            File file1 = new File(config.getString(FILE_PATH_CONFIG_ENTRY) + fileName);
            File file2 = new File(file1.getAbsolutePath() + config.getString(FILE_POSTFIX_CONFIG_ENTRY));

            try {
                logger.info("Starting the dump process");
                FileUtils.writeLines(file1, Collections.singletonList(header));
                FileUtils.writeLines(file1, records, true);
                FileUtils.writeLines(file1, Collections.singletonList(footer), true);

                FileUtils.writeLines(file2, Collections.EMPTY_LIST);

                success = true;
            } catch (IOException e) {
                logger.error("Encountered an error while dumping the records into the file {}. Deleting the old file and retrying again...", fileName, e);
                FileUtils.deleteQuietly(file1);
                FileUtils.deleteQuietly(file2);
            }
        }

        logger.info("Completed the dump process with {} " + (numberOfAttempts > 1 ? "attempts" : "attempt"), numberOfAttempts);
    }

    @Override
    public void processRecords(Collection<MESSAGE> records, boolean async) {
        Objects.requireNonNull(records);

        if (async) {
            threadPoolExecutor.submit(() -> {
                for (MESSAGE record : records) {
                    processRecord(record);
                }
            });
        } else {
            for (MESSAGE record : records) {
                processRecord(record);
            }
        }
    }

    private void processRecord(MESSAGE record) {
        Objects.requireNonNull(record);

        try {
            boolean added = this.pendingRecordsQueue.offer(record);
            while (!added) {
                /*
                In order to be able to add the record into the queue, we need to wait until the queue has capacity.
                As a result, we determine how much time left until the next call to dumpRecords() takes place.
                In addition to that time, we also add 1 second to allocate extra time for processing.
                 */
                long waitTimeMillisUntilQueueHasCapacity = (lastDumpTime.get() + config.getInt(DUMP_INTERVAL_IN_SECONDS_CONFIG_ENTRY) * 1000L) - DateTimeUtils.currentTimeMillis() + 1000L;
                logger.warn("The capacity of internal queue has been reached: {}. Waiting for at least {} seconds or less for the dump to unload internal queue",
                        config.getInt(INTERNAL_QUEUE_CAPACITY_CONFIG_ENTRY),
                        waitTimeMillisUntilQueueHasCapacity / 1000);

                added = this.pendingRecordsQueue.offer(record, waitTimeMillisUntilQueueHasCapacity, TimeUnit.SECONDS);
            }
        } catch (Throwable e) {
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            } else {
                logger.error("Uncaught exception while adding pending record", e);

                if (e instanceof OutOfMemoryError) {
                    logger.error("OutOfMemoryError encountered while saving the record into internal queue");
                }
            }
        }
    }
}
