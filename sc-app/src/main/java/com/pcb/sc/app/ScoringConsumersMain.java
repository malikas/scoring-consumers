package com.pcb.sc.app;

import com.pcb.commons.util.config.ConfigLoader;
import com.pcb.sc.app.config.ScApplicationConfig;
import com.pcb.sc.app.kafka.worker.WorkerType;
import com.pcb.sc.app.kafka.worker.cis20.Cis20WorkerFactory;
import com.pcb.sc.config.WorkerConfig;
import com.pcb.sc.kafka.worker.DefaultWorkerManager;
import com.pcb.sc.kafka.worker.WorkerFactory;
import com.pcb.sc.kafka.worker.WorkerManager;
import org.apache.commons.cli.*;
import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

public class ScoringConsumersMain {

    private static final Logger logger = LoggerFactory.getLogger(ScoringConsumersMain.class);

    private static final String PROPERTIES_FILE = "sc.properties";

    private static WorkerManager workerManager;
    private static CountDownLatch shutdownLatch;
    private static final Map<WorkerType, ? extends WorkerFactory<?, ?>> supportedWorkerFactories = new HashMap<>() {{
        put(WorkerType.CIS20, new Cis20WorkerFactory());
    }};

    public static void main(String[] args) {
        Options options = new Options();

        Option helpCliOption = new Option("h", "help", false, "display help");
        helpCliOption.setRequired(false);
        options.addOption(helpCliOption);

        Option configPathCliOption = new Option("cp", "configPath", true, "configuration path where all required config files are stored");
        configPathCliOption.setRequired(true);
        configPathCliOption.setArgName("CONFIG_PATH");
        configPathCliOption.setArgs(1);
        options.addOption(configPathCliOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter helpFormatter = new HelpFormatter();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            handleArgumentError(e.getMessage(), helpFormatter, options);
            return;
        }

        if (cmd.hasOption(helpCliOption.getOpt())) {
            helpFormatter.printHelp("ScoringConsumersMain", options);
            System.exit(0);
            return;
        }

        // Set configPath retrieved from cmd argument
        String configPath = cmd.getOptionValue(configPathCliOption.getOpt());
        System.setProperty("configPath", configPath);

        // Set default uncaught exception handler
        setDefaultUncaughtExceptionHandler();

        logger.info("_STARTING_");

        shutdownLatch = new CountDownLatch(1);
        Configuration config = initConfig();
        ScApplicationConfig appConfig = initAppConfig(config);
        Set<WorkerType> requiredWorkerTypes = appConfig.workerConfigs().stream().map(WorkerConfig::workerType).collect(Collectors.toSet());
        Map<WorkerType, WorkerFactory<?, ?>> workerFactories = supportedWorkerFactories.entrySet().stream().filter(e -> requiredWorkerTypes.contains(e.getKey())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        initShutdownHook();

        workerManager = new DefaultWorkerManager(appConfig, workerFactories);
        workerManager.start();
        logger.info("_STARTED_");

        try {
            logger.info("Acquiring shutdownLatch");
            shutdownLatch.await();
        } catch (InterruptedException e) {
            logger.error("The main thread was interrupted while awaiting the countdown for shutdownLatch", e);
            logger.info("_SHUTDOWN_FAILED_");
        }
    }

    private static Configuration initConfig() {
        logger.info("Creating Configs");
        return ConfigLoader.getConfigsFromFile(PROPERTIES_FILE);
    }

    private static ScApplicationConfig initAppConfig(Configuration config) {
        logger.info("Creating ScApplicationConfig");
        return new ScApplicationConfig(config);
    }

    private static void initShutdownHook() {
        logger.info("Creating ShutdownHook");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("_SHUTDOWN_INITIATED_");
            workerManager.shutdown();

            logger.info("Releasing shutdownLatch");
            shutdownLatch.countDown();

            logger.info("Successfully released shutdownLatch");
            logger.info("_SHUTDOWN_READY_");
        }));
    }

    private static void setDefaultUncaughtExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            logger.error("Uncaught exception occurred in thread {}", thread, throwable);
        });
    }

    private static void handleArgumentError(String message, HelpFormatter helpFormatter, Options options) {
        Objects.requireNonNull(message);
        Objects.requireNonNull(helpFormatter);
        Objects.requireNonNull(options);

        System.err.println(message);
        helpFormatter.printHelp("ScoringConsumersMain", options);
        System.exit(1);
    }
}
