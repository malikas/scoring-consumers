package com.pcb.sc.app.service;

import java.util.Collection;

public interface RecordsService<MESSAGE> {

    default void start() {
    }

    default void shutdown() {
    }

    void processRecords(Collection<MESSAGE> records, boolean async);
}
