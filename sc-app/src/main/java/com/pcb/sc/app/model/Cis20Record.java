package com.pcb.sc.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JsonDeserialize(builder = Cis20Record.Builder.class)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Cis20Record {
    private static final Logger logger = LoggerFactory.getLogger(Cis20Record.class);

    private String workflow;
    private String recordType;
    private String dataSpecificationVersion;
    private String clientIdFromHeader;
    private Integer recordCreationDate;
    private Integer recordCreationTime;
    private Integer recordCreationMilliseconds;
    private Integer gmtOffset;
    private String customerIdFromHeader;
    private String customerAcctNumber;
    private String externalTransactionId;
    private String customerType;
    private String vipType;
    private Integer relationshipStartDate;
    private Integer numberOfAccounts;
    private String givenName;
    private String middleName;
    private String surname;
    private String title;
    private String suffix;
    private String preferredGreeting;
    private String preferredLanguage;
    private String mothersMaidenName;
    private String householdName;
    private String streetLine1;
    private String streetLine2;
    private String streetLine3;
    private String streetLine4;
    private String city;
    private String stateProvince;
    private String postalCode;
    private String countryCode;
    private String residenceStatus;
    private Integer dateAtAddress;
    private String secondaryAddrType;
    private String secondaryAddrStreetLine1;
    private String secondaryAddrStreetLine2;
    private String secondaryAddrStreetLine3;
    private String secondaryAddrStreetLine4;
    private String secondaryAddrCity;
    private String secondaryAddrStateProvince;
    private String secondaryAddrPostalCode;
    private String secondaryAddrCountryCode;
    private String employer;
    private String workAddrStreetLine1;
    private String workAddrStreetLine2;
    private String workAddrStreetLine3;
    private String workAddrStreetLine4;
    private String workAddrCity;
    private String workAddrStateProvince;
    private String workAddrPostalCode;
    private String workAddrCountryCode;
    private String employmentStatus;
    private Integer employmentStartDate;
    private String employerMcc;
    private String occupationCode;
    private Integer income;
    private String currencyCode;
    private Integer currencyConversionRate;
    private String homePhone;
    private String secondaryPhone;
    private String workPhone;
    private String mobilePhone;
    private String preferredPhone;
    private String emailAddress;
    private String educationalStatus;
    private Integer birthDate;
    private String birthCountry;
    private String citizenshipCountry;
    private String nationalId;
    private String nationalIdCountry;
    private String passportNumber;
    private String passportCountry;
    private Integer passportExpirationDate;
    private String driversLicenseNumber;
    private String driversLicenseCountry;
    private String taxId;
    private String taxIdCountry;
    private String gender;
    private String maritalStatus;
    private Integer numberOfDependents;
    private Integer creditScore;
    private Integer creditScoreDate;
    private String creditScoreSource;
    private String creditScoreRequestReason;
    private String creditRating;
    private String pefp;
    private String ofac;
    private Integer behaviorScore1;
    private Integer behaviorScore2;
    private String segmentId1;
    private String segmentId2;
    private String segmentId3;
    private String segmentId4;
    private String userIndicator01;
    private String userIndicator02;
    private String userIndicator03;
    private String userIndicator04;
    private String userIndicator05;
    private String userCode1;
    private String userCode2;
    private String userCode3;
    private String userCode4;
    private String userCode5;
    private String userData01;
    private String userData02;
    private String userData03;
    private String userData04;
    private String userData05;
    private String userData06;
    private String userData07;
    private String userData08;
    private String userData09;
    private String userData10;
    private String userData11;
    private String userData12;
    private String userData13;
    private String userData14;
    private String userData15;
    private String RESERVED_01;

    private Cis20Record(Builder builder) {
        this.userCode5 = builder.userCode5;
        this.recordCreationTime = builder.recordCreationTime;
        this.customerType = builder.customerType;
        this.employmentStatus = builder.employmentStatus;
        this.educationalStatus = builder.educationalStatus;
        this.creditScoreRequestReason = builder.creditScoreRequestReason;
        this.workAddrStreetLine2 = builder.workAddrStreetLine2;
        this.behaviorScore2 = builder.behaviorScore2;
        this.clientIdFromHeader = builder.clientIdFromHeader;
        this.behaviorScore1 = builder.behaviorScore1;
        this.secondaryAddrStateProvince = builder.secondaryAddrStateProvince;
        this.customerAcctNumber = builder.customerAcctNumber;
        this.driversLicenseCountry = builder.driversLicenseCountry;
        this.userData05 = builder.userData05;
        this.dateAtAddress = builder.dateAtAddress;
        this.occupationCode = builder.occupationCode;
        this.income = builder.income;
        this.secondaryAddrCity = builder.secondaryAddrCity;
        this.dataSpecificationVersion = builder.dataSpecificationVersion;
        this.emailAddress = builder.emailAddress;
        this.userIndicator01 = builder.userIndicator01;
        this.streetLine4 = builder.streetLine4;
        this.pefp = builder.pefp;
        this.preferredPhone = builder.preferredPhone;
        this.givenName = builder.givenName;
        this.maritalStatus = builder.maritalStatus;
        this.secondaryAddrPostalCode = builder.secondaryAddrPostalCode;
        this.workflow = builder.workflow;
        this.birthDate = builder.birthDate;
        this.userData07 = builder.userData07;
        this.employerMcc = builder.employerMcc;
        this.userData06 = builder.userData06;
        this.recordCreationDate = builder.recordCreationDate;
        this.segmentId2 = builder.segmentId2;
        this.streetLine3 = builder.streetLine3;
        this.passportCountry = builder.passportCountry;
        this.secondaryAddrStreetLine3 = builder.secondaryAddrStreetLine3;
        this.customerIdFromHeader = builder.customerIdFromHeader;
        this.homePhone = builder.homePhone;
        this.birthCountry = builder.birthCountry;
        this.ofac = builder.ofac;
        this.city = builder.city;
        this.workAddrStreetLine3 = builder.workAddrStreetLine3;
        this.workAddrStreetLine4 = builder.workAddrStreetLine4;
        this.segmentId4 = builder.segmentId4;
        this.userData04 = builder.userData04;
        this.currencyConversionRate = builder.currencyConversionRate;
        this.creditScoreSource = builder.creditScoreSource;
        this.userData12 = builder.userData12;
        this.mobilePhone = builder.mobilePhone;
        this.gender = builder.gender;
        this.numberOfDependents = builder.numberOfDependents;
        this.numberOfAccounts = builder.numberOfAccounts;
        this.preferredLanguage = builder.preferredLanguage;
        this.secondaryAddrCountryCode = builder.secondaryAddrCountryCode;
        this.userData13 = builder.userData13;
        this.userData01 = builder.userData01;
        this.taxIdCountry = builder.taxIdCountry;
        this.userCode1 = builder.userCode1;
        this.nationalIdCountry = builder.nationalIdCountry;
        this.recordType = builder.recordType;
        this.creditRating = builder.creditRating;
        this.userIndicator02 = builder.userIndicator02;
        this.secondaryAddrStreetLine2 = builder.secondaryAddrStreetLine2;
        this.streetLine2 = builder.streetLine2;
        this.suffix = builder.suffix;
        this.employmentStartDate = builder.employmentStartDate;
        this.secondaryAddrStreetLine1 = builder.secondaryAddrStreetLine1;
        this.countryCode = builder.countryCode;
        this.vipType = builder.vipType;
        this.relationshipStartDate = builder.relationshipStartDate;
        this.externalTransactionId = builder.externalTransactionId;
        this.streetLine1 = builder.streetLine1;
        this.userData15 = builder.userData15;
        this.workAddrCountryCode = builder.workAddrCountryCode;
        this.nationalId = builder.nationalId;
        this.userData09 = builder.userData09;
        this.passportExpirationDate = builder.passportExpirationDate;
        this.postalCode = builder.postalCode;
        this.RESERVED_01 = builder.RESERVED_01;
        this.secondaryAddrStreetLine4 = builder.secondaryAddrStreetLine4;
        this.recordCreationMilliseconds = builder.recordCreationMilliseconds;
        this.workAddrStreetLine1 = builder.workAddrStreetLine1;
        this.userCode2 = builder.userCode2;
        this.userData08 = builder.userData08;
        this.gmtOffset = builder.gmtOffset;
        this.userData10 = builder.userData10;
        this.userData11 = builder.userData11;
        this.userCode3 = builder.userCode3;
        this.userData03 = builder.userData03;
        this.passportNumber = builder.passportNumber;
        this.userData14 = builder.userData14;
        this.userIndicator05 = builder.userIndicator05;
        this.surname = builder.surname;
        this.userIndicator04 = builder.userIndicator04;
        this.userData02 = builder.userData02;
        this.segmentId3 = builder.segmentId3;
        this.workAddrCity = builder.workAddrCity;
        this.userCode4 = builder.userCode4;
        this.currencyCode = builder.currencyCode;
        this.mothersMaidenName = builder.mothersMaidenName;
        this.middleName = builder.middleName;
        this.userIndicator03 = builder.userIndicator03;
        this.preferredGreeting = builder.preferredGreeting;
        this.stateProvince = builder.stateProvince;
        this.creditScoreDate = builder.creditScoreDate;
        this.workPhone = builder.workPhone;
        this.citizenshipCountry = builder.citizenshipCountry;
        this.workAddrPostalCode = builder.workAddrPostalCode;
        this.segmentId1 = builder.segmentId1;
        this.secondaryPhone = builder.secondaryPhone;
        this.workAddrStateProvince = builder.workAddrStateProvince;
        this.title = builder.title;
        this.taxId = builder.taxId;
        this.householdName = builder.householdName;
        this.employer = builder.employer;
        this.secondaryAddrType = builder.secondaryAddrType;
        this.creditScore = builder.creditScore;
        this.residenceStatus = builder.residenceStatus;
        this.driversLicenseNumber = builder.driversLicenseNumber;
    }

    public String getWorkflow() {
        return workflow;
    }

    public String getRecordType() {
        return recordType;
    }

    public String getDataSpecificationVersion() {
        return dataSpecificationVersion;
    }

    public String getClientIdFromHeader() {
        return clientIdFromHeader;
    }

    public Integer getRecordCreationDate() {
        return recordCreationDate;
    }

    public Integer getRecordCreationTime() {
        return recordCreationTime;
    }

    public Integer getRecordCreationMilliseconds() {
        return recordCreationMilliseconds;
    }

    public Integer getGmtOffset() {
        return gmtOffset;
    }

    public String getCustomerIdFromHeader() {
        return customerIdFromHeader;
    }

    public String getCustomerAcctNumber() {
        return customerAcctNumber;
    }

    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public String getVipType() {
        return vipType;
    }

    public Integer getRelationshipStartDate() {
        return relationshipStartDate;
    }

    public Integer getNumberOfAccounts() {
        return numberOfAccounts;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getSurname() {
        return surname;
    }

    public String getTitle() {
        return title;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getPreferredGreeting() {
        return preferredGreeting;
    }

    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public String getHouseholdName() {
        return householdName;
    }

    public String getStreetLine1() {
        return streetLine1;
    }

    public String getStreetLine2() {
        return streetLine2;
    }

    public String getStreetLine3() {
        return streetLine3;
    }

    public String getStreetLine4() {
        return streetLine4;
    }

    public String getCity() {
        return city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getResidenceStatus() {
        return residenceStatus;
    }

    public Integer getDateAtAddress() {
        return dateAtAddress;
    }

    public String getSecondaryAddrType() {
        return secondaryAddrType;
    }

    public String getSecondaryAddrStreetLine1() {
        return secondaryAddrStreetLine1;
    }

    public String getSecondaryAddrStreetLine2() {
        return secondaryAddrStreetLine2;
    }

    public String getSecondaryAddrStreetLine3() {
        return secondaryAddrStreetLine3;
    }

    public String getSecondaryAddrStreetLine4() {
        return secondaryAddrStreetLine4;
    }

    public String getSecondaryAddrCity() {
        return secondaryAddrCity;
    }

    public String getSecondaryAddrStateProvince() {
        return secondaryAddrStateProvince;
    }

    public String getSecondaryAddrPostalCode() {
        return secondaryAddrPostalCode;
    }

    public String getSecondaryAddrCountryCode() {
        return secondaryAddrCountryCode;
    }

    public String getEmployer() {
        return employer;
    }

    public String getWorkAddrStreetLine1() {
        return workAddrStreetLine1;
    }

    public String getWorkAddrStreetLine2() {
        return workAddrStreetLine2;
    }

    public String getWorkAddrStreetLine3() {
        return workAddrStreetLine3;
    }

    public String getWorkAddrStreetLine4() {
        return workAddrStreetLine4;
    }

    public String getWorkAddrCity() {
        return workAddrCity;
    }

    public String getWorkAddrStateProvince() {
        return workAddrStateProvince;
    }

    public String getWorkAddrPostalCode() {
        return workAddrPostalCode;
    }

    public String getWorkAddrCountryCode() {
        return workAddrCountryCode;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public Integer getEmploymentStartDate() {
        return employmentStartDate;
    }

    public String getEmployerMcc() {
        return employerMcc;
    }

    public String getOccupationCode() {
        return occupationCode;
    }

    public Integer getIncome() {
        return income;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Integer getCurrencyConversionRate() {
        return currencyConversionRate;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public String getSecondaryPhone() {
        return secondaryPhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getPreferredPhone() {
        return preferredPhone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getEducationalStatus() {
        return educationalStatus;
    }

    public Integer getBirthDate() {
        return birthDate;
    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public String getCitizenshipCountry() {
        return citizenshipCountry;
    }

    public String getNationalId() {
        return nationalId;
    }

    public String getNationalIdCountry() {
        return nationalIdCountry;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public String getPassportCountry() {
        return passportCountry;
    }

    public Integer getPassportExpirationDate() {
        return passportExpirationDate;
    }

    public String getDriversLicenseNumber() {
        return driversLicenseNumber;
    }

    public String getDriversLicenseCountry() {
        return driversLicenseCountry;
    }

    public String getTaxId() {
        return taxId;
    }

    public String getTaxIdCountry() {
        return taxIdCountry;
    }

    public String getGender() {
        return gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public Integer getNumberOfDependents() {
        return numberOfDependents;
    }

    public Integer getCreditScore() {
        return creditScore;
    }

    public Integer getCreditScoreDate() {
        return creditScoreDate;
    }

    public String getCreditScoreSource() {
        return creditScoreSource;
    }

    public String getCreditScoreRequestReason() {
        return creditScoreRequestReason;
    }

    public String getCreditRating() {
        return creditRating;
    }

    public String getPefp() {
        return pefp;
    }

    public String getOfac() {
        return ofac;
    }

    public Integer getBehaviorScore1() {
        return behaviorScore1;
    }

    public Integer getBehaviorScore2() {
        return behaviorScore2;
    }

    public String getSegmentId1() {
        return segmentId1;
    }

    public String getSegmentId2() {
        return segmentId2;
    }

    public String getSegmentId3() {
        return segmentId3;
    }

    public String getSegmentId4() {
        return segmentId4;
    }

    public String getUserIndicator01() {
        return userIndicator01;
    }

    public String getUserIndicator02() {
        return userIndicator02;
    }

    public String getUserIndicator03() {
        return userIndicator03;
    }

    public String getUserIndicator04() {
        return userIndicator04;
    }

    public String getUserIndicator05() {
        return userIndicator05;
    }

    public String getUserCode1() {
        return userCode1;
    }

    public String getUserCode2() {
        return userCode2;
    }

    public String getUserCode3() {
        return userCode3;
    }

    public String getUserCode4() {
        return userCode4;
    }

    public String getUserCode5() {
        return userCode5;
    }

    public String getUserData01() {
        return userData01;
    }

    public String getUserData02() {
        return userData02;
    }

    public String getUserData03() {
        return userData03;
    }

    public String getUserData04() {
        return userData04;
    }

    public String getUserData05() {
        return userData05;
    }

    public String getUserData06() {
        return userData06;
    }

    public String getUserData07() {
        return userData07;
    }

    public String getUserData08() {
        return userData08;
    }

    public String getUserData09() {
        return userData09;
    }

    public String getUserData10() {
        return userData10;
    }

    public String getUserData11() {
        return userData11;
    }

    public String getUserData12() {
        return userData12;
    }

    public String getUserData13() {
        return userData13;
    }

    public String getUserData14() {
        return userData14;
    }

    public String getUserData15() {
        return userData15;
    }

    public String getRESERVED_01() {
        return RESERVED_01;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Cis20Record{");
        sb.append("workflow='").append(workflow).append('\'');
        sb.append(", recordType='").append(recordType).append('\'');
        sb.append(", dataSpecificationVersion='").append(dataSpecificationVersion).append('\'');
        sb.append(", clientIdFromHeader='").append(clientIdFromHeader).append('\'');
        sb.append(", recordCreationDate=").append(recordCreationDate);
        sb.append(", recordCreationTime=").append(recordCreationTime);
        sb.append(", recordCreationMilliseconds=").append(recordCreationMilliseconds);
        sb.append(", gmtOffset=").append(gmtOffset);
        sb.append(", customerIdFromHeader='").append(customerIdFromHeader).append('\'');
        sb.append(", customerAcctNumber='").append(customerAcctNumber).append('\'');
        sb.append(", externalTransactionId='").append(externalTransactionId).append('\'');
        sb.append(", customerType='").append(customerType).append('\'');
        sb.append(", vipType='").append(vipType).append('\'');
        sb.append(", relationshipStartDate=").append(relationshipStartDate);
        sb.append(", numberOfAccounts=").append(numberOfAccounts);
        sb.append(", givenName='").append(givenName).append('\'');
        sb.append(", middleName='").append(middleName).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", suffix='").append(suffix).append('\'');
        sb.append(", preferredGreeting='").append(preferredGreeting).append('\'');
        sb.append(", preferredLanguage='").append(preferredLanguage).append('\'');
        sb.append(", mothersMaidenName='").append(mothersMaidenName).append('\'');
        sb.append(", householdName='").append(householdName).append('\'');
        sb.append(", streetLine1='").append(streetLine1).append('\'');
        sb.append(", streetLine2='").append(streetLine2).append('\'');
        sb.append(", streetLine3='").append(streetLine3).append('\'');
        sb.append(", streetLine4='").append(streetLine4).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", stateProvince='").append(stateProvince).append('\'');
        sb.append(", postalCode='").append(postalCode).append('\'');
        sb.append(", countryCode='").append(countryCode).append('\'');
        sb.append(", residenceStatus='").append(residenceStatus).append('\'');
        sb.append(", dateAtAddress=").append(dateAtAddress);
        sb.append(", secondaryAddrType='").append(secondaryAddrType).append('\'');
        sb.append(", secondaryAddrStreetLine1='").append(secondaryAddrStreetLine1).append('\'');
        sb.append(", secondaryAddrStreetLine2='").append(secondaryAddrStreetLine2).append('\'');
        sb.append(", secondaryAddrStreetLine3='").append(secondaryAddrStreetLine3).append('\'');
        sb.append(", secondaryAddrStreetLine4='").append(secondaryAddrStreetLine4).append('\'');
        sb.append(", secondaryAddrCity='").append(secondaryAddrCity).append('\'');
        sb.append(", secondaryAddrStateProvince='").append(secondaryAddrStateProvince).append('\'');
        sb.append(", secondaryAddrPostalCode='").append(secondaryAddrPostalCode).append('\'');
        sb.append(", secondaryAddrCountryCode='").append(secondaryAddrCountryCode).append('\'');
        sb.append(", employer='").append(employer).append('\'');
        sb.append(", workAddrStreetLine1='").append(workAddrStreetLine1).append('\'');
        sb.append(", workAddrStreetLine2='").append(workAddrStreetLine2).append('\'');
        sb.append(", workAddrStreetLine3='").append(workAddrStreetLine3).append('\'');
        sb.append(", workAddrStreetLine4='").append(workAddrStreetLine4).append('\'');
        sb.append(", workAddrCity='").append(workAddrCity).append('\'');
        sb.append(", workAddrStateProvince='").append(workAddrStateProvince).append('\'');
        sb.append(", workAddrPostalCode='").append(workAddrPostalCode).append('\'');
        sb.append(", workAddrCountryCode='").append(workAddrCountryCode).append('\'');
        sb.append(", employmentStatus='").append(employmentStatus).append('\'');
        sb.append(", employmentStartDate=").append(employmentStartDate);
        sb.append(", employerMcc='").append(employerMcc).append('\'');
        sb.append(", occupationCode='").append(occupationCode).append('\'');
        sb.append(", income=").append(income);
        sb.append(", currencyCode='").append(currencyCode).append('\'');
        sb.append(", currencyConversionRate=").append(currencyConversionRate);
        sb.append(", homePhone='").append(homePhone).append('\'');
        sb.append(", secondaryPhone='").append(secondaryPhone).append('\'');
        sb.append(", workPhone='").append(workPhone).append('\'');
        sb.append(", mobilePhone='").append(mobilePhone).append('\'');
        sb.append(", preferredPhone='").append(preferredPhone).append('\'');
        sb.append(", emailAddress='").append(emailAddress).append('\'');
        sb.append(", educationalStatus='").append(educationalStatus).append('\'');
        sb.append(", birthDate=").append(birthDate);
        sb.append(", birthCountry='").append(birthCountry).append('\'');
        sb.append(", citizenshipCountry='").append(citizenshipCountry).append('\'');
        sb.append(", nationalId='").append(nationalId).append('\'');
        sb.append(", nationalIdCountry='").append(nationalIdCountry).append('\'');
        sb.append(", passportNumber='").append(passportNumber).append('\'');
        sb.append(", passportCountry='").append(passportCountry).append('\'');
        sb.append(", passportExpirationDate=").append(passportExpirationDate);
        sb.append(", driversLicenseNumber='").append(driversLicenseNumber).append('\'');
        sb.append(", driversLicenseCountry='").append(driversLicenseCountry).append('\'');
        sb.append(", taxId='").append(taxId).append('\'');
        sb.append(", taxIdCountry='").append(taxIdCountry).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", maritalStatus='").append(maritalStatus).append('\'');
        sb.append(", numberOfDependents=").append(numberOfDependents);
        sb.append(", creditScore=").append(creditScore);
        sb.append(", creditScoreDate=").append(creditScoreDate);
        sb.append(", creditScoreSource='").append(creditScoreSource).append('\'');
        sb.append(", creditScoreRequestReason='").append(creditScoreRequestReason).append('\'');
        sb.append(", creditRating='").append(creditRating).append('\'');
        sb.append(", pefp='").append(pefp).append('\'');
        sb.append(", ofac='").append(ofac).append('\'');
        sb.append(", behaviorScore1=").append(behaviorScore1);
        sb.append(", behaviorScore2=").append(behaviorScore2);
        sb.append(", segmentId1='").append(segmentId1).append('\'');
        sb.append(", segmentId2='").append(segmentId2).append('\'');
        sb.append(", segmentId3='").append(segmentId3).append('\'');
        sb.append(", segmentId4='").append(segmentId4).append('\'');
        sb.append(", userIndicator01='").append(userIndicator01).append('\'');
        sb.append(", userIndicator02='").append(userIndicator02).append('\'');
        sb.append(", userIndicator03='").append(userIndicator03).append('\'');
        sb.append(", userIndicator04='").append(userIndicator04).append('\'');
        sb.append(", userIndicator05='").append(userIndicator05).append('\'');
        sb.append(", userCode1='").append(userCode1).append('\'');
        sb.append(", userCode2='").append(userCode2).append('\'');
        sb.append(", userCode3='").append(userCode3).append('\'');
        sb.append(", userCode4='").append(userCode4).append('\'');
        sb.append(", userCode5='").append(userCode5).append('\'');
        sb.append(", userData01='").append(userData01).append('\'');
        sb.append(", userData02='").append(userData02).append('\'');
        sb.append(", userData03='").append(userData03).append('\'');
        sb.append(", userData04='").append(userData04).append('\'');
        sb.append(", userData05='").append(userData05).append('\'');
        sb.append(", userData06='").append(userData06).append('\'');
        sb.append(", userData07='").append(userData07).append('\'');
        sb.append(", userData08='").append(userData08).append('\'');
        sb.append(", userData09='").append(userData09).append('\'');
        sb.append(", userData10='").append(userData10).append('\'');
        sb.append(", userData11='").append(userData11).append('\'');
        sb.append(", userData12='").append(userData12).append('\'');
        sb.append(", userData13='").append(userData13).append('\'');
        sb.append(", userData14='").append(userData14).append('\'');
        sb.append(", userData15='").append(userData15).append('\'');
        sb.append(", RESERVED_01='").append(RESERVED_01).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class Builder {
        private String workflow;
        private String recordType;
        private String dataSpecificationVersion;
        private String clientIdFromHeader;
        private Integer recordCreationDate;
        private Integer recordCreationTime;
        private Integer recordCreationMilliseconds;
        private Integer gmtOffset;
        private String customerIdFromHeader;
        private String customerAcctNumber;
        private String externalTransactionId;
        private String customerType;
        private String vipType;
        private Integer relationshipStartDate;
        private Integer numberOfAccounts;
        private String givenName;
        private String middleName;
        private String surname;
        private String title;
        private String suffix;
        private String preferredGreeting;
        private String preferredLanguage;
        private String mothersMaidenName;
        private String householdName;
        private String streetLine1;
        private String streetLine2;
        private String streetLine3;
        private String streetLine4;
        private String city;
        private String stateProvince;
        private String postalCode;
        private String countryCode;
        private String residenceStatus;
        private Integer dateAtAddress;
        private String secondaryAddrType;
        private String secondaryAddrStreetLine1;
        private String secondaryAddrStreetLine2;
        private String secondaryAddrStreetLine3;
        private String secondaryAddrStreetLine4;
        private String secondaryAddrCity;
        private String secondaryAddrStateProvince;
        private String secondaryAddrPostalCode;
        private String secondaryAddrCountryCode;
        private String employer;
        private String workAddrStreetLine1;
        private String workAddrStreetLine2;
        private String workAddrStreetLine3;
        private String workAddrStreetLine4;
        private String workAddrCity;
        private String workAddrStateProvince;
        private String workAddrPostalCode;
        private String workAddrCountryCode;
        private String employmentStatus;
        private Integer employmentStartDate;
        private String employerMcc;
        private String occupationCode;
        private Integer income;
        private String currencyCode;
        private Integer currencyConversionRate;
        private String homePhone;
        private String secondaryPhone;
        private String workPhone;
        private String mobilePhone;
        private String preferredPhone;
        private String emailAddress;
        private String educationalStatus;
        private Integer birthDate;
        private String birthCountry;
        private String citizenshipCountry;
        private String nationalId;
        private String nationalIdCountry;
        private String passportNumber;
        private String passportCountry;
        private Integer passportExpirationDate;
        private String driversLicenseNumber;
        private String driversLicenseCountry;
        private String taxId;
        private String taxIdCountry;
        private String gender;
        private String maritalStatus;
        private Integer numberOfDependents;
        private Integer creditScore;
        private Integer creditScoreDate;
        private String creditScoreSource;
        private String creditScoreRequestReason;
        private String creditRating;
        private String pefp;
        private String ofac;
        private Integer behaviorScore1;
        private Integer behaviorScore2;
        private String segmentId1;
        private String segmentId2;
        private String segmentId3;
        private String segmentId4;
        private String userIndicator01;
        private String userIndicator02;
        private String userIndicator03;
        private String userIndicator04;
        private String userIndicator05;
        private String userCode1;
        private String userCode2;
        private String userCode3;
        private String userCode4;
        private String userCode5;
        private String userData01;
        private String userData02;
        private String userData03;
        private String userData04;
        private String userData05;
        private String userData06;
        private String userData07;
        private String userData08;
        private String userData09;
        private String userData10;
        private String userData11;
        private String userData12;
        private String userData13;
        private String userData14;
        private String userData15;
        private String RESERVED_01;

        private Builder() {
        }

        public Builder workflow(String workflow) {
            this.workflow = workflow;
            return this;
        }

        public Builder recordType(String recordType) {
            this.recordType = recordType;
            return this;
        }

        public Builder dataSpecificationVersion(String dataSpecificationVersion) {
            this.dataSpecificationVersion = dataSpecificationVersion;
            return this;
        }

        public Builder clientIdFromHeader(String clientIdFromHeader) {
            this.clientIdFromHeader = clientIdFromHeader;
            return this;
        }

        public Builder recordCreationDate(String recordCreationDate) {
            recordCreationDate = recordCreationDate.trim();
            if (!recordCreationDate.isBlank()) {
                try {
                    Integer recordCreationDateInt = Integer.parseInt(recordCreationDate);
                    return recordCreationDate(recordCreationDateInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. relationshipStartDate: {}", recordCreationDate);
                    return this;
                }
            }

            return this;
        }

        public Builder recordCreationDate(Integer recordCreationDate) {
            this.recordCreationDate = recordCreationDate;
            return this;
        }

        public Builder recordCreationTime(String recordCreationTime) {
            recordCreationTime = recordCreationTime.trim();
            if (!recordCreationTime.isBlank()) {
                try {
                    Integer recordCreationTimeInt = Integer.parseInt(recordCreationTime);
                    return recordCreationTime(recordCreationTimeInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. recordCreationTime: {}", recordCreationTime);
                    return this;
                }
            }

            return this;
        }

        public Builder recordCreationTime(Integer recordCreationTime) {
            this.recordCreationTime = recordCreationTime;
            return this;
        }

        public Builder recordCreationMilliseconds(String recordCreationMilliseconds) {
            recordCreationMilliseconds = recordCreationMilliseconds.trim();
            if (!recordCreationMilliseconds.isBlank()) {
                try {
                    Integer recordCreationMillisecondsInt = Integer.parseInt(recordCreationMilliseconds);
                    return recordCreationMilliseconds(recordCreationMillisecondsInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. recordCreationMilliseconds: {}", recordCreationMilliseconds);
                    return this;
                }
            }

            return this;
        }

        public Builder recordCreationMilliseconds(Integer recordCreationMilliseconds) {
            this.recordCreationMilliseconds = recordCreationMilliseconds;
            return this;
        }

        public Builder gmtOffset(String gmtOffset) {
            gmtOffset = gmtOffset.trim();
            if (!gmtOffset.isBlank()) {
                try {
                    Integer gmtOffsetInt = Integer.parseInt(gmtOffset);
                    return gmtOffset(gmtOffsetInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. gmtOffset: {}", gmtOffset);
                    return this;
                }
            }

            return this;
        }

        public Builder gmtOffset(Integer gmtOffset) {
            this.gmtOffset = gmtOffset;
            return this;
        }

        public Builder customerIdFromHeader(String customerIdFromHeader) {
            this.customerIdFromHeader = customerIdFromHeader;
            return this;
        }

        public Builder customerAcctNumber(String customerAcctNumber) {
            this.customerAcctNumber = customerAcctNumber;
            return this;
        }

        public Builder externalTransactionId(String externalTransactionId) {
            this.externalTransactionId = externalTransactionId;
            return this;
        }

        public Builder customerType(String customerType) {
            this.customerType = customerType;
            return this;
        }

        public Builder vipType(String vipType) {
            this.vipType = vipType;
            return this;
        }

        public Builder relationshipStartDate(String relationshipStartDate) {
            relationshipStartDate = relationshipStartDate.trim();
            if (!relationshipStartDate.isBlank()) {
                try {
                    Integer relationshipStartDateInt = Integer.parseInt(relationshipStartDate);
                    return relationshipStartDate(relationshipStartDateInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. relationshipStartDate: {}", relationshipStartDate);
                    return this;
                }
            }

            return this;
        }

        public Builder relationshipStartDate(Integer relationshipStartDate) {
            this.relationshipStartDate = relationshipStartDate;
            return this;
        }

        public Builder numberOfAccounts(String numberOfAccounts) {
            numberOfAccounts = numberOfAccounts.trim();
            if (!numberOfAccounts.isBlank()) {
                try {
                    Integer numberOfAccountsInt = Integer.parseInt(numberOfAccounts);
                    return numberOfAccounts(numberOfAccountsInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. numberOfAccounts: {}", numberOfAccounts);
                    return this;
                }
            }

            return this;
        }

        public Builder numberOfAccounts(Integer numberOfAccounts) {
            this.numberOfAccounts = numberOfAccounts;
            return this;
        }

        public Builder givenName(String givenName) {
            this.givenName = givenName;
            return this;
        }

        public Builder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder suffix(String suffix) {
            this.suffix = suffix;
            return this;
        }

        public Builder preferredGreeting(String preferredGreeting) {
            this.preferredGreeting = preferredGreeting;
            return this;
        }

        public Builder preferredLanguage(String preferredLanguage) {
            this.preferredLanguage = preferredLanguage;
            return this;
        }

        public Builder mothersMaidenName(String mothersMaidenName) {
            this.mothersMaidenName = mothersMaidenName;
            return this;
        }

        public Builder householdName(String householdName) {
            this.householdName = householdName;
            return this;
        }

        public Builder streetLine1(String streetLine1) {
            this.streetLine1 = streetLine1;
            return this;
        }

        public Builder streetLine2(String streetLine2) {
            this.streetLine2 = streetLine2;
            return this;
        }

        public Builder streetLine3(String streetLine3) {
            this.streetLine3 = streetLine3;
            return this;
        }

        public Builder streetLine4(String streetLine4) {
            this.streetLine4 = streetLine4;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder stateProvince(String stateProvince) {
            this.stateProvince = stateProvince;
            return this;
        }

        public Builder postalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public Builder countryCode(String countryCode) {
            this.countryCode = countryCode;
            return this;
        }

        public Builder residenceStatus(String residenceStatus) {
            this.residenceStatus = residenceStatus;
            return this;
        }

        public Builder dateAtAddress(String dateAtAddress) {
            dateAtAddress = dateAtAddress.trim();
            if (!dateAtAddress.isBlank()) {
                try {
                    Integer dateAtAddressInt = Integer.parseInt(dateAtAddress);
                    return dateAtAddress(dateAtAddressInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. dateAtAddress: {}", dateAtAddress);
                    return this;
                }
            }

            return this;
        }

        public Builder dateAtAddress(Integer dateAtAddress) {
            this.dateAtAddress = dateAtAddress;
            return this;
        }

        public Builder secondaryAddrType(String secondaryAddrType) {
            this.secondaryAddrType = secondaryAddrType;
            return this;
        }

        public Builder secondaryAddrStreetLine1(String secondaryAddrStreetLine1) {
            this.secondaryAddrStreetLine1 = secondaryAddrStreetLine1;
            return this;
        }

        public Builder secondaryAddrStreetLine2(String secondaryAddrStreetLine2) {
            this.secondaryAddrStreetLine2 = secondaryAddrStreetLine2;
            return this;
        }

        public Builder secondaryAddrStreetLine3(String secondaryAddrStreetLine3) {
            this.secondaryAddrStreetLine3 = secondaryAddrStreetLine3;
            return this;
        }

        public Builder secondaryAddrStreetLine4(String secondaryAddrStreetLine4) {
            this.secondaryAddrStreetLine4 = secondaryAddrStreetLine4;
            return this;
        }

        public Builder secondaryAddrCity(String secondaryAddrCity) {
            this.secondaryAddrCity = secondaryAddrCity;
            return this;
        }

        public Builder secondaryAddrStateProvince(String secondaryAddrStateProvince) {
            this.secondaryAddrStateProvince = secondaryAddrStateProvince;
            return this;
        }

        public Builder secondaryAddrPostalCode(String secondaryAddrPostalCode) {
            this.secondaryAddrPostalCode = secondaryAddrPostalCode;
            return this;
        }

        public Builder secondaryAddrCountryCode(String secondaryAddrCountryCode) {
            this.secondaryAddrCountryCode = secondaryAddrCountryCode;
            return this;
        }

        public Builder employer(String employer) {
            this.employer = employer;
            return this;
        }

        public Builder workAddrStreetLine1(String workAddrStreetLine1) {
            this.workAddrStreetLine1 = workAddrStreetLine1;
            return this;
        }

        public Builder workAddrStreetLine2(String workAddrStreetLine2) {
            this.workAddrStreetLine2 = workAddrStreetLine2;
            return this;
        }

        public Builder workAddrStreetLine3(String workAddrStreetLine3) {
            this.workAddrStreetLine3 = workAddrStreetLine3;
            return this;
        }

        public Builder workAddrStreetLine4(String workAddrStreetLine4) {
            this.workAddrStreetLine4 = workAddrStreetLine4;
            return this;
        }

        public Builder workAddrCity(String workAddrCity) {
            this.workAddrCity = workAddrCity;
            return this;
        }

        public Builder workAddrStateProvince(String workAddrStateProvince) {
            this.workAddrStateProvince = workAddrStateProvince;
            return this;
        }

        public Builder workAddrPostalCode(String workAddrPostalCode) {
            this.workAddrPostalCode = workAddrPostalCode;
            return this;
        }

        public Builder workAddrCountryCode(String workAddrCountryCode) {
            this.workAddrCountryCode = workAddrCountryCode;
            return this;
        }

        public Builder employmentStatus(String employmentStatus) {
            this.employmentStatus = employmentStatus;
            return this;
        }

        public Builder employmentStartDate(String employmentStartDate) {
            employmentStartDate = employmentStartDate.trim();
            if (!employmentStartDate.isBlank()) {
                try {
                    Integer employmentStartDateInt = Integer.parseInt(employmentStartDate);
                    return employmentStartDate(employmentStartDateInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. employmentStartDate: {}", employmentStartDate);
                    return this;
                }
            }

            return this;
        }

        public Builder employmentStartDate(Integer employmentStartDate) {
            this.employmentStartDate = employmentStartDate;
            return this;
        }

        public Builder employerMcc(String employerMcc) {
            this.employerMcc = employerMcc;
            return this;
        }

        public Builder occupationCode(String occupationCode) {
            this.occupationCode = occupationCode;
            return this;
        }

        public Builder income(String income) {
            income = income.trim();
            if (!income.isBlank()) {
                try {
                    Integer incomeInt = Integer.parseInt(income);
                    return income(incomeInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. income: {}", income);
                    return this;
                }
            }

            return this;
        }

        public Builder income(Integer income) {
            this.income = income;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder currencyConversionRate(String currencyConversionRate) {
            currencyConversionRate = currencyConversionRate.trim();
            if (!currencyConversionRate.isBlank()) {
                try {
                    Integer currencyConversionRateInt = Integer.parseInt(currencyConversionRate);
                    return currencyConversionRate(currencyConversionRateInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. currencyConversionRate: {}", currencyConversionRate);
                    return this;
                }
            }

            return this;
        }

        public Builder currencyConversionRate(Integer currencyConversionRate) {
            this.currencyConversionRate = currencyConversionRate;
            return this;
        }

        public Builder homePhone(String homePhone) {
            this.homePhone = homePhone;
            return this;
        }

        public Builder secondaryPhone(String secondaryPhone) {
            this.secondaryPhone = secondaryPhone;
            return this;
        }

        public Builder workPhone(String workPhone) {
            this.workPhone = workPhone;
            return this;
        }

        public Builder mobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
            return this;
        }

        public Builder preferredPhone(String preferredPhone) {
            this.preferredPhone = preferredPhone;
            return this;
        }

        public Builder emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        public Builder educationalStatus(String educationalStatus) {
            this.educationalStatus = educationalStatus;
            return this;
        }

        public Builder birthDate(String birthDate) {
            birthDate = birthDate.trim();
            if (!birthDate.isBlank()) {
                try {
                    Integer birthDateInt = Integer.parseInt(birthDate);
                    return birthDate(birthDateInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. birthDate: {}", birthDate);
                    return this;
                }
            }

            return this;
        }

        public Builder birthDate(Integer birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Builder birthCountry(String birthCountry) {
            this.birthCountry = birthCountry;
            return this;
        }

        public Builder citizenshipCountry(String citizenshipCountry) {
            this.citizenshipCountry = citizenshipCountry;
            return this;
        }

        public Builder nationalId(String nationalId) {
            this.nationalId = nationalId;
            return this;
        }

        public Builder nationalIdCountry(String nationalIdCountry) {
            this.nationalIdCountry = nationalIdCountry;
            return this;
        }

        public Builder passportNumber(String passportNumber) {
            this.passportNumber = passportNumber;
            return this;
        }

        public Builder passportCountry(String passportCountry) {
            this.passportCountry = passportCountry;
            return this;
        }

        public Builder passportExpirationDate(String passportExpirationDate) {
            passportExpirationDate = passportExpirationDate.trim();
            if (!passportExpirationDate.isBlank()) {
                try {
                    Integer passportExpirationDateInt = Integer.parseInt(passportExpirationDate);
                    return passportExpirationDate(passportExpirationDateInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. passportExpirationDate: {}", passportExpirationDate);
                    return this;
                }
            }

            return this;
        }

        public Builder passportExpirationDate(Integer passportExpirationDate) {
            this.passportExpirationDate = passportExpirationDate;
            return this;
        }

        public Builder driversLicenseNumber(String driversLicenseNumber) {
            this.driversLicenseNumber = driversLicenseNumber;
            return this;
        }

        public Builder driversLicenseCountry(String driversLicenseCountry) {
            this.driversLicenseCountry = driversLicenseCountry;
            return this;
        }

        public Builder taxId(String taxId) {
            this.taxId = taxId;
            return this;
        }

        public Builder taxIdCountry(String taxIdCountry) {
            this.taxIdCountry = taxIdCountry;
            return this;
        }

        public Builder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder maritalStatus(String maritalStatus) {
            this.maritalStatus = maritalStatus;
            return this;
        }

        public Builder numberOfDependents(String numberOfDependents) {
            numberOfDependents = numberOfDependents.trim();
            if (!numberOfDependents.isBlank()) {
                try {
                    Integer numberOfDependentsInt = Integer.parseInt(numberOfDependents);
                    return numberOfDependents(numberOfDependentsInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. numberOfDependents: {}", numberOfDependents);
                    return this;
                }
            }

            return this;
        }

        public Builder numberOfDependents(Integer numberOfDependents) {
            this.numberOfDependents = numberOfDependents;
            return this;
        }

        public Builder creditScore(String creditScore) {
            creditScore = creditScore.trim();
            if (!creditScore.isBlank()) {
                try {
                    Integer creditScoreInt = Integer.parseInt(creditScore);
                    return creditScore(creditScoreInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. creditScore: {}", creditScore);
                    return this;
                }
            }

            return this;
        }

        public Builder creditScore(Integer creditScore) {
            this.creditScore = creditScore;
            return this;
        }

        public Builder creditScoreDate(String creditScoreDate) {
            creditScoreDate = creditScoreDate.trim();
            if (!creditScoreDate.isBlank()) {
                try {
                    Integer creditScoreDateInt = Integer.parseInt(creditScoreDate);
                    return creditScoreDate(creditScoreDateInt);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. creditScoreDate: {}", creditScoreDate);
                    return this;
                }
            }

            return this;
        }

        public Builder creditScoreDate(Integer creditScoreDate) {
            this.creditScoreDate = creditScoreDate;
            return this;
        }

        public Builder creditScoreSource(String creditScoreSource) {
            this.creditScoreSource = creditScoreSource;
            return this;
        }

        public Builder creditScoreRequestReason(String creditScoreRequestReason) {
            this.creditScoreRequestReason = creditScoreRequestReason;
            return this;
        }

        public Builder creditRating(String creditRating) {
            this.creditRating = creditRating;
            return this;
        }

        public Builder pefp(String pefp) {
            this.pefp = pefp;
            return this;
        }

        public Builder ofac(String ofac) {
            this.ofac = ofac;
            return this;
        }

        public Builder behaviorScore1(String behaviorScore1) {
            behaviorScore1 = behaviorScore1.trim();
            if (!behaviorScore1.isBlank()) {
                try {
                    Integer behaviorScore1Int = Integer.parseInt(behaviorScore1);
                    return behaviorScore1(behaviorScore1Int);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. behaviorScore1: {}", behaviorScore1);
                    return this;
                }
            }

            return this;
        }

        public Builder behaviorScore1(Integer behaviorScore1) {
            this.behaviorScore1 = behaviorScore1;
            return this;
        }

        public Builder behaviorScore2(String behaviorScore2) {
            behaviorScore2 = behaviorScore2.trim();
            if (!behaviorScore2.isBlank()) {
                try {
                    Integer behaviorScore2Int = Integer.parseInt(behaviorScore2);
                    return behaviorScore2(behaviorScore2Int);
                } catch (NumberFormatException e) {
                    logger.warn("Couldn't parse the field value as it doesn't contain parsable integer. behaviorScore2: {}", behaviorScore2);
                    return this;
                }
            }

            return this;
        }

        public Builder behaviorScore2(Integer behaviorScore2) {
            this.behaviorScore2 = behaviorScore2;
            return this;
        }

        public Builder segmentId1(String segmentId1) {
            this.segmentId1 = segmentId1;
            return this;
        }

        public Builder segmentId2(String segmentId2) {
            this.segmentId2 = segmentId2;
            return this;
        }

        public Builder segmentId3(String segmentId3) {
            this.segmentId3 = segmentId3;
            return this;
        }

        public Builder segmentId4(String segmentId4) {
            this.segmentId4 = segmentId4;
            return this;
        }

        public Builder userIndicator01(String userIndicator01) {
            this.userIndicator01 = userIndicator01;
            return this;
        }

        public Builder userIndicator02(String userIndicator02) {
            this.userIndicator02 = userIndicator02;
            return this;
        }

        public Builder userIndicator03(String userIndicator03) {
            this.userIndicator03 = userIndicator03;
            return this;
        }

        public Builder userIndicator04(String userIndicator04) {
            this.userIndicator04 = userIndicator04;
            return this;
        }

        public Builder userIndicator05(String userIndicator05) {
            this.userIndicator05 = userIndicator05;
            return this;
        }

        public Builder userCode1(String userCode1) {
            this.userCode1 = userCode1;
            return this;
        }

        public Builder userCode2(String userCode2) {
            this.userCode2 = userCode2;
            return this;
        }

        public Builder userCode3(String userCode3) {
            this.userCode3 = userCode3;
            return this;
        }

        public Builder userCode4(String userCode4) {
            this.userCode4 = userCode4;
            return this;
        }

        public Builder userCode5(String userCode5) {
            this.userCode5 = userCode5;
            return this;
        }

        public Builder userData01(String userData01) {
            this.userData01 = userData01;
            return this;
        }

        public Builder userData02(String userData02) {
            this.userData02 = userData02;
            return this;
        }

        public Builder userData03(String userData03) {
            this.userData03 = userData03;
            return this;
        }

        public Builder userData04(String userData04) {
            this.userData04 = userData04;
            return this;
        }

        public Builder userData05(String userData05) {
            this.userData05 = userData05;
            return this;
        }

        public Builder userData06(String userData06) {
            this.userData06 = userData06;
            return this;
        }

        public Builder userData07(String userData07) {
            this.userData07 = userData07;
            return this;
        }

        public Builder userData08(String userData08) {
            this.userData08 = userData08;
            return this;
        }

        public Builder userData09(String userData09) {
            this.userData09 = userData09;
            return this;
        }

        public Builder userData10(String userData10) {
            this.userData10 = userData10;
            return this;
        }

        public Builder userData11(String userData11) {
            this.userData11 = userData11;
            return this;
        }

        public Builder userData12(String userData12) {
            this.userData12 = userData12;
            return this;
        }

        public Builder userData13(String userData13) {
            this.userData13 = userData13;
            return this;
        }

        public Builder userData14(String userData14) {
            this.userData14 = userData14;
            return this;
        }

        public Builder userData15(String userData15) {
            this.userData15 = userData15;
            return this;
        }

        public Builder RESERVED_01(String RESERVED_01) {
            this.RESERVED_01 = RESERVED_01;
            return this;
        }

        public Cis20Record build() {
            return new Cis20Record(this);
        }
    }
}
