package com.pcb.sc.app.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.MoreExecutors;
import com.pcb.sc.app.model.Cis20Record;
import com.pcb.sc.app.model.ErrorRecord;
import com.pcb.sc.app.parser.RecordParser;
import com.pcb.sc.app.service.rest.ScoringServerService;
import com.pcb.sc.app.service.RecordsService;
import okhttp3.RequestBody;
import okio.Buffer;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.io.FileUtils;
import org.glassfish.jersey.internal.guava.ThreadFactoryBuilder;
import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class RestCallRecordsService<RAW, PARSED> implements RecordsService<RAW> {

    private static final Logger logger = LoggerFactory.getLogger(RestCallRecordsService.class);

    private static final String CONFIG_KEY_PREFIX = "RestCallRecordsService.";

    private static final String SCORING_SERVER_URL_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "scoringServerUrl";
    private static final String FILE_ERROR_PATH_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "fileErrorPath";
    private static final String FILE_ERROR_NAME_CONFIG_ENTRY = CONFIG_KEY_PREFIX + "fileErrorName";

    private final Configuration config;

    private final RecordParser<RAW, PARSED> recordParser;
    private final ExecutorService threadPoolExecutor;
    private final ScoringServerService scoringServerService;

    private final AtomicBoolean shutdown = new AtomicBoolean();

    public RestCallRecordsService(@Nonnull Configuration config, @Nonnull RecordParser<RAW, PARSED> recordParser) {
        this.config = Preconditions.checkNotNull(config, "config cannot be null");
        this.recordParser = Preconditions.checkNotNull(recordParser, "recordParser cannot be null");

        this.threadPoolExecutor = Executors.newFixedThreadPool(20, new ThreadFactoryBuilder().setNameFormat("RestCallRecordsServiceThreadPoolExecutor-%d").build());

        scoringServerService = new Retrofit.Builder()
                .baseUrl(config.getString(SCORING_SERVER_URL_CONFIG_ENTRY))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ScoringServerService.class);
    }

    @Override
    public void shutdown() {
        if (shutdown.getAndSet(true)) {
            return;
        }

        MoreExecutors.shutdownAndAwaitTermination(threadPoolExecutor, 1, TimeUnit.MINUTES);
    }

    @Override
    public void processRecords(Collection<RAW> records, boolean async) {
        Objects.requireNonNull(records);

        if (async) {
            threadPoolExecutor.submit(() -> {
                for (RAW record : records) {
                    processRecord(record);
                }
            });
        } else {
            for (RAW record : records) {
                processRecord(record);
            }
        }
    }

    private void processRecord(RAW record) {
        Objects.requireNonNull(record);
        PARSED parsedRecord = this.recordParser.parseRecord(record);

        Call<Void> restCall;
        if (parsedRecord instanceof Cis20Record) {
            restCall = scoringServerService.publishRecord((Cis20Record) parsedRecord);
        } else {
            logger.warn("Unsupported record has been received -- no mapping can be found. Skipping the record");
            return;
        }

        try {
            Response<Void> response = restCall.execute();
            if (response.isSuccessful()) {
                logger.info("The record was sent successfully. The response code is {}", response.code());
            } else {
                logger.warn("The record wasn't sent successfully. Saving the ErrorRecord into the file {}", config.getString(FILE_ERROR_NAME_CONFIG_ENTRY));
                saveErrorRecord(
                        ErrorRecord.builder()
                                .requestTimestamp(DateTimeUtils.currentTimeMillis())
                                .requestBody(getRequestBody(restCall.request().body()))
                                .responseCode(response.code())
                                .build()
                );
            }
        } catch (IOException e) {
            logger.error("Encountered an error while sending a record");
            throw new RuntimeException(e);
        }
    }

    private void saveErrorRecord(ErrorRecord errorRecord) {
        boolean success = false;
        int numberOfAttempts = 0;

        while (!success) {
            numberOfAttempts++;

            File file = new File(config.getString(FILE_ERROR_PATH_CONFIG_ENTRY) + config.getString(FILE_ERROR_NAME_CONFIG_ENTRY));
            try {
                FileUtils.writeLines(file, Collections.singletonList(errorRecord.toString()), true);

                success = true;
            } catch (IOException e) {
                logger.error("Encountered an error while saving the ErrorRecord into the file {}. Retrying again...", config.getString(FILE_ERROR_NAME_CONFIG_ENTRY), e);
            }
        }

        logger.trace("Completed the save process with {} " + (numberOfAttempts > 1 ? "attempts" : "attempt"), numberOfAttempts);
    }

    private String getRequestBody(RequestBody requestBody) {
        if (requestBody != null) {
            Buffer buffer = new Buffer();
            try {
                requestBody.writeTo(buffer);
                return buffer.readUtf8();
            } catch (IOException e) {
                logger.error("Couldn't retrieve request body", e);
            }
        }

        return "";
    }
}
