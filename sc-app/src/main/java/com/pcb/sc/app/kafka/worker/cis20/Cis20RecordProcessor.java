package com.pcb.sc.app.kafka.worker.cis20;

import com.pcb.kafkacore.consumer.model.ConsumerRecord;
import com.pcb.sc.app.kafka.worker.AbstractBatchRecordProcessor;
import com.pcb.sc.app.service.RecordsService;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Cis20RecordProcessor extends AbstractBatchRecordProcessor<String, Cis20WorkerConfig> {

    private final RecordsService<String> batchFileRecordsService;
    private final RecordsService<String> restCallRecordsService;

    Cis20RecordProcessor(@Nonnull Cis20WorkerConfig workerConfig, @Nonnull RecordsService<String> batchFileRecordsService, @Nonnull RecordsService<String> restCallRecordsService) {
        super(workerConfig);
        this.batchFileRecordsService = batchFileRecordsService;
        this.restCallRecordsService = restCallRecordsService;
    }

    @Override
    protected boolean isValid(ConsumerRecord<String, String> record) {
        return record != null
                && record.value() != null;
    }

    /**
     * Used to process a single record. RestCallRecordsService is used as the underlying processor. However, please
     * note that this method is not intended to be called under a heavy load, i.e., the underlying
     * RestCallRecordsService might not be able to handle much throughput.
     *
     * @param s Single record's value
     */
    @Override
    protected void process(String s) {
        restCallRecordsService.processRecords(Collections.singletonList(s), false);
    }

    /**
     * Used to process a batch of records. Depending on the number of records in current batch, the underlying
     * RecordsService will be used. If there are more than maxNumberOfRecordsForRestCall records, pass the records
     * to the BatchFileRecordsService. Otherwise, RestCallRecordsService will be used for further processing.
     *
     * @param records List of ConsumerRecords to process
     */
    @Override
    protected void process(List<ConsumerRecord<String, String>> records) {
        if (!records.isEmpty()) {
            List<String> recordValues = records.stream().map(ConsumerRecord::value).collect(Collectors.toList());

            if (records.size() > workerConfig.restCallMaxNumberOfRecords()) {
                batchFileRecordsService.processRecords(recordValues, false);
            } else {
                restCallRecordsService.processRecords(recordValues, false);
            }
        }

        clear();
    }

    @Override
    public void shutdown() {
        super.shutdown();

        logger.info("Shutting down batchFileRecordsService");
        batchFileRecordsService.shutdown();
        logger.info("Shutting down restCallRecordsService");
        restCallRecordsService.shutdown();
    }
}
