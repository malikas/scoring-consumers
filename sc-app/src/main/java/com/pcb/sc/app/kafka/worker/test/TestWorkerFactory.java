package com.pcb.sc.app.kafka.worker.test;

import com.pcb.kafkacore.common.serde.StringDeserializer;
import com.pcb.sc.app.config.ScWorkerConfig;
import com.pcb.sc.config.WorkerConfig;
import com.pcb.sc.kafka.worker.AbstractWorkerFactory;
import com.pcb.sc.service.BatchRecordProcessor;

import javax.annotation.Nonnull;

public class TestWorkerFactory extends AbstractWorkerFactory<String, String> {

    public TestWorkerFactory() {
        super(new StringDeserializer(), new StringDeserializer());
    }

    @Override
    protected BatchRecordProcessor<String, String> recordProcessor(@Nonnull WorkerConfig workerConfig) {
        return new TestRecordProcessor(new ScWorkerConfig(workerConfig));
    }
}
