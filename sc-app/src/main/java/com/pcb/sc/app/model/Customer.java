package com.pcb.sc.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonPropertyOrder(alphabetic = true)
@JsonDeserialize(builder = Customer.Builder.class)
public class Customer {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private String clubStatus;
    private String comments;
    private Long createTs;
    private Long updateTs;

    private Customer(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
        this.gender = builder.gender;
        this.clubStatus = builder.clubStatus;
        this.comments = builder.comments;
        this.createTs = builder.createTs;
        this.updateTs = builder.updateTs;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public String getClubStatus() {
        return clubStatus;
    }

    public String getComments() {
        return comments;
    }

    public Long getCreateTs() {
        return createTs;
    }

    public Long getUpdateTs() {
        return updateTs;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Customer{");
        sb.append("id=").append(id);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", clubStatus='").append(clubStatus).append('\'');
        sb.append(", comments='").append(comments).append('\'');
        sb.append(", createTs=").append(createTs);
        sb.append(", updateTs=").append(updateTs);
        sb.append('}');
        return sb.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class Builder {
        @JsonProperty("ID")
        private Integer id;
        @JsonProperty("FIRST_NAME")
        private String firstName;
        @JsonProperty("LAST_NAME")
        private String lastName;
        @JsonProperty("EMAIL")
        private String email;
        @JsonProperty("GENDER")
        private String gender;
        @JsonProperty("CLUB_STATUS")
        private String clubStatus;
        @JsonProperty("COMMENTS")
        private String comments;
        @JsonProperty("CREATE_TS")
        private Long createTs;
        @JsonProperty("UPDATE_TS")
        private Long updateTs;

        private Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder clubStatus(String clubStatus) {
            this.clubStatus = clubStatus;
            return this;
        }

        public Builder comments(String comments) {
            this.comments = comments;
            return this;
        }

        public Builder createTs(Long createTs) {
            this.createTs = createTs;
            return this;
        }

        public Builder updateTs(Long updateTs) {
            this.updateTs = updateTs;
            return this;
        }

        public Customer build() {
            return new Customer(this);
        }
    }
}
