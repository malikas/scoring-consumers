package com.pcb.sc.app.kafka.worker.cis20;

import com.pcb.kafkacore.common.serde.StringDeserializer;
import com.pcb.sc.app.parser.impl.Cis20RecordParser;
import com.pcb.sc.config.WorkerConfig;
import com.pcb.sc.kafka.worker.AbstractWorkerFactory;
import com.pcb.sc.service.BatchRecordProcessor;
import com.pcb.sc.app.service.impl.BatchFileRecordsService;
import com.pcb.sc.app.service.impl.RestCallRecordsService;
import org.jetbrains.annotations.NotNull;

public class Cis20WorkerFactory extends AbstractWorkerFactory<String, String> {

    public Cis20WorkerFactory() {
        super(new StringDeserializer(), new StringDeserializer());
    }

    @Override
    protected BatchRecordProcessor<String, String> recordProcessor(@NotNull WorkerConfig workerConfig) {
        return new Cis20RecordProcessor(
                new Cis20WorkerConfig(workerConfig.underlyingConfig(), workerConfig.workerId(), workerConfig.workerType(), workerConfig.namespace()),
                new BatchFileRecordsService<>(workerConfig.underlyingConfig()),
                new RestCallRecordsService<>(workerConfig.underlyingConfig(), new Cis20RecordParser())
        );
    }
}
